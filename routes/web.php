<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/home', 'HomeController@index')->name('home');
Route::get('/hit', 'HomeController@push_to_apns');
Auth::routes();


Route::group(['middleware' => 'auth'], function() {
    Route::get('/plans', 'PlanController@index')->name('plans.index');
    Route::get('/plan/{plan}', 'PlanController@show')->name('plans.show');
    Route::get('/braintree/token', 'BraintreeTokenController@index')->name('token');
     Route::post('/subscription', 'SubscriptionController@create')->name('subscription.create');

     Route::get('stripe', 'StripePaymentController@stripe');
     Route::post('stripe/post', 'StripePaymentController@stripePost')->name('stripe.post');
     Route::get('stripe/create', 'StripePaymentController@create');
     Route::get('stripe/update', 'StripePaymentController@update');
     Route::get('stripe/retreive', 'StripePaymentController@retreive');
     Route::get('link', 'StripePaymentController@link');
     Route::get('createNew', 'StripePaymentController@createNew');
});

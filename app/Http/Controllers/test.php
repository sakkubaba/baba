<?php

namespace App\Http\Controllers\Api;

use App\Helpers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\Commission;
use App\Models\DriverAddressInfo;
use App\Models\DriverInfo;
use App\Models\DriverRating;
use App\Models\DriverStripe;
use App\Models\FAQ;
use App\Models\MoneySplitDetail;
use App\Models\NotificationTemplate;
use App\Models\Notifications;
use App\Models\RideDetails;
use App\Models\RideLatLng;
use App\Models\TaxiDriverInfo;
use App\Models\TaxiTypes;
use App\Models\User;
use App\Models\UserCards;
use App\Models\UserDeviceInfo;
use App\Models\UserFavouriteDriver;
use App\Models\UserRating;
use App\Models\UserStripe;
use App\Models\UserWallet;
use App\Models\WaitingCharges;
use App\Traits\Redirection;
use DateTime;
use Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Sly\NotificationPusher\Adapter\Apns as ApnsAdapter;
use Sly\NotificationPusher\Collection\DeviceCollection;
use Sly\NotificationPusher\Model\Device;
use Sly\NotificationPusher\Model\Message;
use Sly\NotificationPusher\Model\Push;
use Sly\NotificationPusher\PushManager;
use Twilio;
use Twilio\Exceptions\RestException;
use JWT;
include(app_path().'/Http/Controllers/Ios.php');

class LoginController extends Controller
{
	public $successCode = 200;
    public $errorCode = 202;
    public $limit = 10;
    public $inMilesRadius = 15;

    public function sendSMS(Request $request){
    	
    	$accountId = 'AC97c18c5acc7fc006934740b9937ca258';
        /*$accountId = 'AC82f990e5d5f281550e1f65a4441ae04f';*/
    	$token = '4f718be1ea157a0e427b5964336ea71d';
        /*$token='cc11ed4cabf0f407696c5e4690767e02';*/
    	$fromNumber = '+12015598324';
    	// $fromNumber = null;
    	$twilio = new \Aloha\Twilio\Twilio($accountId, $token, $fromNumber);
         //$response = $twilio->message('+918750469918', 'Hi'); 
		//echo '<pre>'; print_r($response);
    }


    public static function sendSMStoUser($number, $message){
        $accountId = 'AC97c18c5acc7fc006934740b9937ca258';
        /*$accountId = 'AC82f990e5d5f281550e1f65a4441ae04f';*/
        $token = '4f718be1ea157a0e427b5964336ea71d';
        /*$token='cc11ed4cabf0f407696c5e4690767e02';*/
        $fromNumber = '+1 740 313 5114';
       
        $twilio = new \Aloha\Twilio\Twilio($accountId, $token, $fromNumber);
        try {
                $response = $twilio->message('+'.$number, $message); 
                return 'ok';
            } catch (RestException $exception) {
                if ($exception->getCode() === 21211){
                    return 'invalidNumber';
                }
            }
    }

    public function uploadImage(Request $request)
    {
    	$error = true;
    	$validator = Validator::make($request->all(), [
            'image_name' => 'required',
        ]);

    	if (!$validator->fails()) {
    		$error = false;
    		$fileParam = "image";
    		if($request->image_name == 'cardImage')
    		{
    			$saveFolder = "user_cards";

    		}elseif($request->image_name == 'driverProfile')
    		{
    			$saveFolder = "driver_profile";

    		}elseif($request->image_name == 'stripeImage'){

              $saveFolder = "stripe_Image";
            }
            elseif($request->image_name == 'driverLicence')
    		{
    			$saveFolder = "driver_licence";
                
    		}elseif($request->image_name == 'vehicleRegistration')
    		{
    			$saveFolder = "vehicle_registration";
    		}elseif($request->image_name == 'userProfile')
            {
                $saveFolder = "user_profile";
            }else
    		{
    			$error = true;
    		}

    		if($error == false){

    			if (!empty($_FILES[$fileParam])) {
	    			$error = false;
		            $newFileName = rand(111, 999) . time() . '_' . $_FILES[$fileParam]['name'];
		            move_uploaded_file($_FILES[$fileParam]['tmp_name'], __DIR__ . '/../../../../public/images/' . trim($saveFolder, '/') . '/' . $newFileName);

		            $response = ['message'=> 'Image upload successfully.', 'image_name' => $newFileName];
	        	}else{
					$response = ['message'=> 'Image cannot be blank.'];
	        	}

    		}else{
    				$response = ['message'=> 'Image name is not correct.'];
    		}

    	}else{
            
            $messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
        }

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
    }

    public function sendNotification($template_name, $user_id, $extraParameters)
    {
    	if($template_name && $user_id)
    	{
    		$getTemplate = NotificationTemplate::where(['templateName' => $template_name])->first();
    		if($getTemplate){
    			$template = $getTemplate->template;
    			if(!isset($extraParameters['driverName'])){
                    $extraParameters['driverName'] = '';
                }
    			if($template_name == 'driver-ride-request'){

    				$finalTemplate = str_replace("{{from}}",$extraParameters['from'], $template);
                    $finalTemplate = str_replace("{{to}}",$extraParameters['to'], $finalTemplate);
                    $user_type = 'driver';
                }elseif($template_name == 'driver-accept-request'){

                	$finalTemplate = str_replace("{{from}}",$extraParameters['from'], $template);
                    $finalTemplate = str_replace("{{to}}",$extraParameters['to'], $finalTemplate);
                    $finalTemplate = str_replace("{{Username}}",$extraParameters['driverName'], $finalTemplate);
                    $user_type = 'user';
                }elseif($template_name == 'driver-arriving-to-user'){

                    $finalTemplate = str_replace("{{Username}}",$extraParameters['driverName'], $template);
                    $user_type = 'user';
                }elseif($template_name == 'driver-reached-to-user'){
                    
                    $finalTemplate = str_replace(["{{Username}}","{{Waitingtime}}"],[$extraParameters['driverName'],$extraParameters['waitingTime']], $template);

                    $user_type = 'user';
                }elseif($template_name == 'driver-start-for-parcel'){
                    
                    $finalTemplate = str_replace("{{Username}}",$extraParameters['driverName'], $template);
                    $user_type = 'user';
                }elseif($template_name == 'driver-end-for-parcel'){
                    
                    $finalTemplate = str_replace("{{Username}}",$extraParameters['driverName'], $template);
                    $user_type = 'user';
                }elseif($template_name == 'driver-cancelled-ride'){
                    
                    $finalTemplate = str_replace("{{Username}}",$extraParameters['driverName'], $template);
                    $user_type = 'user';
                }elseif($template_name == 'user-cancelled-ride'){
                    
                    $finalTemplate = str_replace("{{Username}}",$extraParameters['driverName'], $template);
                    $user_type = 'driver';
                }elseif($template_name == 'user-ride-amount'){
                    
                    $finalTemplate = str_replace("{{amount}}",$extraParameters['amount'], $template);
                    $user_type = 'user';

                }elseif($template_name == 'payment-cash'){
                    $finalTemplate = str_replace("{{money}}",$extraParameters['money'], $template);
                    $user_type = 'user';                    
                }elseif($template_name == 'payment-wallet'){
                    $finalTemplate = str_replace("{{money}}",$extraParameters['money'], $template);
                    $user_type = 'user';                    
                }elseif($template_name == 'payment-wallet-to-driver'){
                    $finalTemplate = str_replace("{{money}}",$extraParameters['money'], $template);
                    $finalTemplate = str_replace("{{user}}",$extraParameters['user_name'], $finalTemplate);

                    $user_type = 'driver';                    
                }
                 elseif($template_name == 'waiting-time-charge'){
                    $finalTemplate = $template;
                    $user_type = 'user'; 
                 }
                 elseif($template_name == 'user-update-location'){
                    $finalTemplate = str_replace("{{Username}}",$extraParameters['driverName'], $template);
                    $user_type = 'driver';
                 }
                    
                if($finalTemplate)
                {
                    if($extraParameters)
                    {
                    	$extraPara = json_encode($extraParameters);
                    }else{
                    	$extraPara = NULL;
                    }
                    //insert notification in the database table
                    	$addNotification = new Notifications();
                    	$addNotification->fk_user_id = $user_id;
                    	$addNotification->user_type = $user_type;
                    	$addNotification->message = $finalTemplate;
                    	$addNotification->messageRead = '0';
                    	$addNotification->templateName = $template_name;
                    	$addNotification->extra = $extraPara;
                    	$addNotification->addedtime = time();
                    	$addNotification->save();

                    $getUserDeviceToken = UserDeviceInfo::where(['fk_user_id' => $user_id])->get();
                    //dd($getUserDeviceToken[0]);
                        // $getUserDeviceToken = UserDeviceInfo::select(['device_type','device_token'])->where(['fk_user_id' => '563'])->get();
                    if((isset($getUserDeviceToken)) && isset($getUserDeviceToken[0]->device_token))
                    {
                        foreach($getUserDeviceToken as $userDeviceToken){
                            $deviceToken = $userDeviceToken->device_token;
                            $deviceType = $userDeviceToken->device_type;
                            // send notification to all user mobiles
                            if(isset($extraParameters['ride_id'])){
                                $ride_id = $extraParameters['ride_id'];
                            }else{
                                $ride_id = "";
                            }
                            if($deviceType == "android"){
                                      // Log::info(['hello android']);
                                $server_key = 'AAAAetq5TFk:APA91bF1pkqz_4rZ6ZIbKfecpD0IB7TzeXKCVYUV6H95Zf6URLbj36CVmo4KAyo_QL4tvtpOKO6mcjySWiXZmAqbBusPNRDJXwRRoF9W3JRQ25LBCja8NKt4Mq4zrg7SmlnO5F1kkiLd';

                                $url = "https://fcm.googleapis.com/fcm/send";
                                $fields = array("to" => $deviceToken,
                                               // "notification" => array(
                                               //                          "body" => $finalTemplate, 
                                               //                        ),
                                               "data" => array(
                                                                "push_id" => (string)$addNotification->id,
                                                                "type" => (string)$template_name,
                                                                "ride_id" => (string)$ride_id,
                                                                "notification_for" => (string)$user_type,
                                                                "body" => $finalTemplate,
                                                                "title" => "Hello",
                                                                "user_id" => (string)$user_id,
                                                                "read_status" => (int)0,
                                                              )
                                            );

                                $fields = json_encode($fields);
                               // print_r(json_encode($fields, JSON_FORCE_OBJECT)); die();

                                $headers = array(
                                    'Content-Type:application/json',
                                    'Authorization:key='.$server_key
                                );
                              
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                                $result = curl_exec($ch);
                                curl_close($ch);
                                // var_dump($result); die();
                            }else{
                                if( ctype_xdigit($deviceToken) && 64 == strlen($deviceToken)){
                                    if($userDeviceToken->userInfo->user_type == 'user'){
                                 /*     //  Log::info(['hello ios user']);
                                        $pushManager = new PushManager(PushManager::ENVIRONMENT_DEV);
                                        // Then declare an adapter.
                                        $apnsAdapter = new ApnsAdapter(array(
                                            'certificate' => public_path().'/pem/roverUser.pem',
                                        ));
                                        
                                        // Set the device(s) to push the notification to.
                                        $devices = new DeviceCollection(array(
                                            new Device($deviceToken),
                                            // ...
                                        ));

                                        // Then, create the push skel.
                                        // $message = new Message('This is a basic example of push.');
                                        $message = new Message($finalTemplate, array(
                                            'custom' => array('custom data' => 
                                                                array(
                                                                        "push_id" => (string)$addNotification->id,
                                                                        "type" => (string)$template_name,
                                                                        "ride_id" => (string)$ride_id,
                                                                        "notification_for" => (string)$user_type,
                                                                        "body" => $finalTemplate,
                                                                        //"title" => "Hello",
                                                                        "user_id" => (string)$user_id,
                                                                        "read_status" => (int)0,
                                                                     )
                                                            )
                                        ));

                                        // Finally, create and add the push to the manager, and push it!
                                        $push = new Push($apnsAdapter, $devices, $message);
                                        $pushManager->add($push);
                                        $pushManager->push();
                                        $response = $push->getResponses();*/
                                                                             $authKey = storage_path()."/AuthKey_78UACFNZZ6.p8";
                                        $arParam['teamId'] = '7UQ75PB7G4';// Get it from Apple Developer's page
                                        $arParam['authKeyId'] = '78UACFNZZ6';
                                        $arParam['apns-topic'] = 'com.rovertaxi.user';
                                        $arParam['production'] = true;
                                        $arClaim = ['iss'=>$arParam['teamId'], 'iat'=>time()];
                                        $arParam['p_key'] = file_get_contents($authKey);
                                        $arParam['header_jwt'] = JWT::encode($arClaim, $arParam['p_key'], $arParam['authKeyId'], 'RS256');

                                        // Sending a request to APNS
                                        // $stat = push_to_apns($arParam, $ar_msg);
                                        // if($stat == FALSE){
                                        // // err handling
                                        //     exit();
                                        // }

                                        $arSendData = array();

                                        $url_cnt = "https://www.google.com";

                                       // $arSendData['aps']['alert']['title'] = sprintf($finalTemplate);
                                        $arSendData['aps']['sound'] = sprintf("default"); // Notification title
                                        $arSendData['aps']['alert']['body'] = sprintf($finalTemplate); // body text
                                        $arSendData['custom data'] = 
                                                                array(
                                                                        "push_id" => (string)$addNotification->id,
                                                                        "type" => (string)$template_name,
                                                                        "ride_id" => (string)$ride_id,
                                                                        "notification_for" => (string)$user_type,
                                                                        "body" => $finalTemplate,
                                                                        //"title" => "Hello",
                                                                        "user_id" => (string)$user_id,
                                                                        "read_status" => (int)0,
                                                                     );
                                        $sendDataJson = json_encode($arSendData);

                                        $endPoint = 'https://api.push.apple.com/3/device'; // https://api.push.apple.com/3/device

                                        //　Preparing request header for APNS
                                        $ar_request_head[] = sprintf("content-type: application/json");
                                        $ar_request_head[] = sprintf("authorization: bearer %s", $arParam['header_jwt']);
                                        $ar_request_head[] = sprintf("apns-topic: %s", $arParam['apns-topic']);

                                        $dev_token = $deviceToken;  // Device token

                                        $url = sprintf("%s/%s", $endPoint, $dev_token);

                                        $ch = curl_init($url);

                                        curl_setopt($ch, CURLOPT_POSTFIELDS, $sendDataJson);
                                        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
                                        curl_setopt($ch, CURLOPT_HTTPHEADER, $ar_request_head);
                                        $response = curl_exec($ch);
                                        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                                        if(empty(curl_error($ch))){
                                        // echo "empty curl error \n";
                                        }

                                        // Logging
                                      // After we need to remove device tokens which had response code 410.
                                      /*
                                            if(intval($httpcode) == 200){ fwrite($fp_ok, $output); }
                                            else{ fwrite($fp_ng, $output); }
                                            if(intval($httpcode) == 410){ fwrite($fp_410, $output); }
                                      */
                                        curl_close($ch);

                                      }  
                                    else{
                                  // Log::info(['heloios driver']);
                                     /*   $pushManager = new PushManager(PushManager::ENVIRONMENT_DEV);
                                        // Then declare an adapter.
                                        $apnsAdapter = new ApnsAdapter(array(
                                            'certificate' => public_path().'/pem/roverDriver.pem',
                                        ));
                                        
                                        // Set the device(s) to push the notification to.
                                        $devices = new DeviceCollection(array(
                                            new Device($deviceToken),
                                            // ...
                                        ));

                                        // Then, create the push skel.
                                        // $message = new Message('This is a basic example of push.');
                                        $message = new Message($finalTemplate, array(
                                            'custom' => array('custom data' => 
                                                                array(
                                                                        "push_id" => (string)$addNotification->id,
                                                                        "type" => (string)$template_name,
                                                                        "ride_id" => (string)$ride_id,
                                                                        "notification_for" => (string)$user_type,
                                                                        "body" => $finalTemplate,
                                                                        //"title" => "Hello",
                                                                        "user_id" => (string)$user_id,
                                                                        "read_status" => (int)0,
                                                                     )
                                                            )
                                        ));
                                        // Finally, create and add the push to the manager, and push it!
                                        $push = new Push($apnsAdapter, $devices, $message);
                                        $pushManager->add($push);
                                        $pushManager->push();
                                        $response = $push->getResponses();*/

                                         $authKey = storage_path()."/AuthKey_78UACFNZZ6.p8";
                                        $arParam['teamId'] = '7UQ75PB7G4';// Get it from Apple Developer's page
                                        $arParam['authKeyId'] = '78UACFNZZ6';
                                        $arParam['apns-topic'] = 'com.rovertaxi.driver';
                                        $arParam['production'] = true;
                                        $arClaim = ['iss'=>$arParam['teamId'], 'iat'=>time()];
                                        $arParam['p_key'] = file_get_contents($authKey);
                                        $arParam['header_jwt'] = JWT::encode($arClaim, $arParam['p_key'], $arParam['authKeyId'], 'RS256');

                                        // Sending a request to APNS
                                        // $stat = push_to_apns($arParam, $ar_msg);
                                        // if($stat == FALSE){
                                        // // err handling
                                        //     exit();
                                        // }

                                        $arSendData = array();

                                        $url_cnt = "https://www.google.com";
                                       // $arSendData['aps']['alert']['title'] = sprintf($finalTemplate);
                                        $arSendData['aps']['sound'] = sprintf("default"); // Notification title
                                        $arSendData['aps']['alert']['body'] = sprintf($finalTemplate); // body text
                                        $arSendData['custom data'] = 
                                                                array(
                                                                        "push_id" => (string)$addNotification->id,
                                                                        "type" => (string)$template_name,
                                                                        "ride_id" => (string)$ride_id,
                                                                        "notification_for" => (string)$user_type,
                                                                        "body" => $finalTemplate,
                                                                        //"title" => "Hello",
                                                                        "user_id" => (string)$user_id,
                                                                        "read_status" => (int)0,
                                                                     );
                                        $sendDataJson = json_encode($arSendData);

                                        $endPoint = 'https://api.development.push.apple.com/3/device'; // https://api.push.apple.com/3/device

                                        //　Preparing request header for APNS
                                        $ar_request_head[] = sprintf("content-type: application/json");
                                        $ar_request_head[] = sprintf("authorization: bearer %s", $arParam['header_jwt']);
                                        $ar_request_head[] = sprintf("apns-topic: %s", $arParam['apns-topic']);

                                        $dev_token = $deviceToken;  // Device token

                                        $url = sprintf("%s/%s", $endPoint, $dev_token);

                                        $ch = curl_init($url);

                                        curl_setopt($ch, CURLOPT_POSTFIELDS, $sendDataJson);
                                        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
                                        curl_setopt($ch, CURLOPT_HTTPHEADER, $ar_request_head);
                                        $response = curl_exec($ch);
                                        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                                        if(empty(curl_error($ch))){
                                        // echo "empty curl error \n";
                                        }

                                        // Logging
                                      // After we need to remove device tokens which had response code 410.
                                      /*
                                            if(intval($httpcode) == 200){ fwrite($fp_ok, $output); }
                                            else{ fwrite($fp_ng, $output); }
                                            if(intval($httpcode) == 410){ fwrite($fp_410, $output); }
                                      */
                                        curl_close($ch);

                                    }
                                }
                            }

                        }
                    }


                }

    		}
    	}
    }

    public function getAllNotifications(Request $request){
        $error = true;
        $validator = Validator::make($request->all(), [
            'user_type' => 'required',
        ]);
        $user = Auth::User();

        if($user){
            $error = false;
            
            $limit = $this->limit;
            if($request->page_number){
                $pageNumber = $request->page_number;
                if($pageNumber == '1'){
                    $offset = $limit*($pageNumber - 1);
                    
                }elseif($pageNumber > 1){
                    $offset = $limit*($pageNumber - 1);
                }else{
                    $limit = '-1';
                    $offset = '0';
                }
            }else{
                $limit = '100';
                $offset = '0';
            }

        $total_unreard = Notifications::where(['fk_user_id' => $user->id, 'messageRead' => '0'])->count();
        $total_noti = Notifications::where(['fk_user_id' => $user->id])->count();
        // print_r($total_unreard); die();
            $getNotifications = Notifications::where(['fk_user_id' => $user->id])
            ->offset($offset)
            ->limit($limit)
            ->orderBy('addedtime', 'desc')
            ->get();
            $data = [];
            if(isset($getNotifications[0]))
            {
                foreach($getNotifications as $notification){
                    $extra = $notification->extra;
                    $jsondecode = json_decode($extra);
                    if(isset($jsondecode->ride_id)){
                        $ride_id = $jsondecode->ride_id;
                    }else{
                        $ride_id = "";
                    }
                    

                    $data[] = array(
                        'push_id' => $notification->id,
                        'type' => $notification->templateName,
                        'ride_id' => $ride_id,
                        'notification_for' => $user->user_type,
                        'view_push' => "",
                        'user_id' => $notification->fk_user_id,
                        'message' => $notification->message,
                        'read_status' => $notification->messageRead,
                    );

               /* if($user->user_type == 'user'){                   
                    if($notification->messageRead == '0'){
                        $markRead = Notifications::where(['id' => $notification->id])->first();
                        $markRead->messageRead = '1';
                        $markRead->save();
                    }
                }*/
            }
                       
                $response = ['message' => 'Notification list', 'data' => $data, 'total_unread_count' => $total_unreard, 'total_notification' => $total_noti];

            }else{

                $response = ['message' => 'No Notification found'];
            }

        }else {
                
                $response = ['message' => 'User not found.'];
        }

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);	
    }

    public function sendIOSDriverNotification($template_name = null, $user_id = null, $extraParameters = null){
        $pushManager = new PushManager(PushManager::ENVIRONMENT_DEV);
        // Then declare an adapter.
        $apnsAdapter = new ApnsAdapter(array(
            'certificate' => public_path().'/pem/roverDriver.pem',
        ));
        
        // Set the device(s) to push the notification to.
        $devices = new DeviceCollection(array(
            new Device('E31E4590E05418A4ADD47DDCD128EDD1A9044650EEA45289FC4FF548F30B2B88'),
            // ...
        ));

        // Then, create the push skel.
        // $message = new Message('This is a basic example of push.');
        $message = new Message('This is the final message.', array(
            'custom' => array('custom data' => 
                                    array(
                                            "push_id" => '1',
                                            "type" => 'driver-template',
                                            "ride_id" => '1234',
                                            "notification_for" => 'driver',
                                            "body" => 'This is the final message',
                                            "user_id" => '1234',
                                            "read_status" => '0',
                                         )
                            )
        ));

        // Finally, create and add the push to the manager, and push it!
        $push = new Push($apnsAdapter, $devices, $message);
        $pushManager->add($push);
        $pushManager->push();
        $response = $push->getResponses();
        dd($response);
    }


    public function push_to_apns(){
    
    $authKey = storage_path()."/AuthKey_78UACFNZZ6.p8";
    $arParam['teamId'] = '7UQ75PB7G4';// Get it from Apple Developer's page
    $arParam['authKeyId'] = '78UACFNZZ6';
    $arParam['apns-topic'] = 'com.rovertaxi.driver';
    $arParam['production'] = true;
    $arClaim = ['iss'=>$arParam['teamId'], 'iat'=>time()];
    $arParam['p_key'] = file_get_contents($authKey);
    $arParam['header_jwt'] = JWT::encode($arClaim, $arParam['p_key'], $arParam['authKeyId'], 'RS256');

    // Sending a request to APNS
    // $stat = push_to_apns($arParam, $ar_msg);
    // if($stat == FALSE){
    // // err handling
    //     exit();
    // }

    $arSendData = array();

    $url_cnt = "https://www.google.com";
  //  $arSendData['aps']['alert']['title'] = sprintf("Someone has requested for a ride from Roshan Garden, New Delhi, Delhi, India, 110043 to Dwarka, New Delhi, Delhi, India"); // Notification title
    $arSendData['aps']['alert']['body'] = sprintf("Someone has requested for a ride from Roshan Garden, New Delhi, Delhi, India, 110043 to Dwarka, New Delhi, Delhi, India"); // body text
    $arSendData['aps']['sound'] = sprintf("default");
  //  $arSendData['data']['jump-url'] = $url_cnt;
    $arSendData['custom data'] = 
                                    array(
                                            "push_id" => '1',
                                            "type" => 'driver-template',
                                            "ride_id" => '1234',
                                            "notification_for" => 'driver',
                                            "body" => 'This is the final message',
                                            "user_id" => '1234',
                                            "read_status" => '0',
                                         );
                            
     // other parameters to send to the app

    $sendDataJson = json_encode($arSendData);

    $endPoint = 'https://api.development.push.apple.com/3/device'; // https://api.push.apple.com/3/device

    //　Preparing request header for APNS
    $ar_request_head[] = sprintf("content-type: application/json");
    $ar_request_head[] = sprintf("authorization: bearer %s", $arParam['header_jwt']);
    $ar_request_head[] = sprintf("apns-topic: %s", $arParam['apns-topic']);

    $dev_token = 'DECE7BDD19BD3E68D49884F5A45E5D7859B942DA8F1F43B0067F8D6A27F29FAC';  // Device token

    $url = sprintf("%s/%s", $endPoint, $dev_token);

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_POSTFIELDS, $sendDataJson);
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $ar_request_head);
    $response = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if(empty(curl_error($ch))){
    // echo "empty curl error \n";
    }

    // Logging
  // After we need to remove device tokens which had response code 410.
  /*
        if(intval($httpcode) == 200){ fwrite($fp_ok, $output); }
        else{ fwrite($fp_ng, $output); }
        if(intval($httpcode) == 410){ fwrite($fp_410, $output); }
  */
    curl_close($ch);

    return "hi";
}

   	public function sendOtp(Request $request)
	{
		$error = true;
		$validator = Validator::make($request->all(), [
            'phone' => 'required',
            'user_type' => 'required',
            //'deviceId' => 'required',
            'deviceType' => 'required',
           // 'deviceToken' => 'required',
        ]);

		if (!$validator->fails()) {
			$error = false;
			$six_digit_random_number = mt_rand(100000, 999999);
			$checkUser = User::where(['phone' => $request->phone, 'user_type' => $request->user_type])->first();

			if($checkUser)
			{
				$checkNewUser = $checkUser->newUser;
				$checkUser->user_otp = $six_digit_random_number;
				$checkUser->save();

				$userID = $checkUser->id;
			}
			else
			{
				/*if($request->user_type == 'user')
				{
					$getUser = User::where(['phone' => $request->phone, 'user_type' => 'driver'])->first();
					if($getUser)
					{
						$error = true;
						$response = ['message'=> 'This number has already registered with driver.', 'code' => $this->errorCode];
						return response()->json(['response' => $response], $response['code']);

					}

				}elseif($request->user_type == 'driver'){

					$getUser = User::where(['phone' => $request->phone, 'user_type' => 'user'])->first();
					if($getUser)
					{
						$error = true;
						$response = ['message'=> 'This number has already registered with user.', 'code' => $this->errorCode];
						return response()->json(['response' => $response], $response['code']);

					}
				}*/
						

                        $newUser = new User();
						$newUser->phone = $request->phone;
						$newUser->user_type = $request->user_type;
						$newUser->user_otp = $six_digit_random_number;
						$newUser->newUser = 'yes';
						$newUser->save();

						$userID = $newUser->id;
						$checkNewUser = $newUser->newUser;
				
			}

			$checkUserDevice = UserDeviceInfo::where(['device_id' => $request->deviceId, 'fk_user_id' => $userID])->first();

			if(!$checkUserDevice)
			{
                
              if($request->user_type == 'driver'){
                $val = UserDeviceInfo::where(['fk_user_id' =>$userID])->get();
                if(!empty($val)){
       
                   foreach($val as $pp){
                    $pp->delete();
                   }
                }
            }
				$newUserDevice = new UserDeviceInfo();
				$newUserDevice->fk_user_id = $userID;
				$newUserDevice->device_type = $request->deviceType;
                $newUserDevice->device_id = $request->deviceId;
				$newUserDevice->device_token = $request->deviceToken;
				$newUserDevice->save();
			}else{
                $currTime = date("Y-m-d H:i:s");
                $checkUserDevice->updated_at = $currTime;
                $checkUserDevice->device_token = $request->deviceToken;
                $checkUserDevice->save();
            }
                
                if($request->user_type == 'user'){
                    $message = 'Hi! Please use '.$six_digit_random_number.' OTP for login to User App.';
                }elseif($request->user_type == 'driver'){
                    $message = 'Hi! Please use '.$six_digit_random_number.' OTP for login to Driver App.';
                }else{
                    $message = 'Hi! Please use '.$six_digit_random_number.' OTP for login.';    
                }
                
                $twilioResponse = $this->sendSMStoUser($request->phone, $message);
                //dd($twilioResponse);
                $twilioResponse = 'ok';
                if($twilioResponse == 'ok'){
                    $response = ['message'=> 'OTP has been sent.', 'otp' => $six_digit_random_number, 'user_id' =>  $userID,'newUser' => $checkNewUser];
                }else{
                    $error = true;
                    $response = ['message'=> 'Invalid Phone number.'];
                }

		}else{
            
            $messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
        }

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}


	public function verifyOtp(Request $request)
	{
		$error = true;
		$validator = Validator::make($request->all(), [
			'phone' => 'required',
            'user_type' => 'required',
            'otp' => 'required',
		]);

		if(!$validator->fails()){

			$checkUser = User::where(['phone' => $request->phone, 'user_otp' => $request->otp])->first();	
			if ($checkUser) {
                if($checkUser->active == '1'){
    				$error = false;

                    if($checkUser->user_type == 'driver'){
                
                        $response = ['message' => 'OTP is valid.', 'userData' => $this->loginData($checkUser),
                            'driverData' => $this->loginDriverData($checkUser->id)];
                    }else{
    				    $response = ['message' => 'OTP is valid.', 'userData' => $this->loginData($checkUser)];
                    }
                }else{
                    $response = ['message' => 'Your account is currently Inactive. Please contact Admin.'];    
                }
			} else {
				$response = ['message' => 'Invalid OTP.'];
			}
		}
		else {
			$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}

	protected function loginData($userModel) 
	{
        $getWallet = UserWallet::where(['fk_user_id' => $userModel->id])->first();
        if($getWallet)
        {
            $userWallet = $getWallet->wallet_amount;
        }else{
            $userWallet = 0;
        }

        if($userModel->user_image){
            $profileImage = env('APP_URL').'/public/images/user_profile/'.$userModel->user_image;
        }else{
            $profileImage = NULL;
        }

		return [
			'user_id' => $userModel->id,
            'active' => $userModel->active,
			'token' => AppHelper::getUserToken($userModel),
			'name' => $userModel->name,
			'user_type' => $userModel->user_type,
			'email' => $userModel->email,
			'phone' => $userModel->phone,
            'wallet_amount' => $userWallet,
            'user_image' => $profileImage
			// 'deviceInfo' => $userModel->userDeviceInfo
			// 		->map(function($item){
			// 			return ['device_type' => $item->device_type, 'device_token' => $item->device_token];
			// 		})
		];
	}

/*protected function loginDriverData($driverID) 
    {
         $getDriver = DriverInfo::where(['fk_user_id' => $driverID])->first();
        // echo '<pre>'; print_r($getDriver->userDriverInfo); die();
                 //userDeviceInfo
                 //userDriverInfo
                 //taxiDriverInfo
                 //taxiTypes
                 //taxiDriverAddressInfo  
        $taxi_driver=TaxiDriverInfo::where(['fk_user_id'=>$driverID])->get();
         if($taxi_driver->count() == 2){
            $taxi1=TaxiTypes::where(['id'=>$taxi_driver[0]->fk_taxi_type_id])->first();
            $taxi2=TaxiTypes::where(['id'=>$taxi_driver[1]->fk_taxi_type_id])->first();

            $taxiTypeInfo1 =array(
                            'taxi_type' => $taxi1->taxi_type,
                            'taxi_name' => (!$getDriver->taxiDriverInfo->taxiTypes) ? '':$getDriver->taxiDriverInfo->taxiTypes->taxi_name,
                            'base_price' => $taxi1->base_price,
                            'price_per_km' => $taxi1->price_per_km,
                            'taxi_image' => env('APP_URL').'/public/images/taxi/'.$taxi1->taxi_image,
                           'capacity' => (!$getDriver->taxiDriverInfo->taxiTypes) ? '':$getDriver->taxiDriverInfo->taxiTypes->capacity,
                            'waiting_charges' => (!$getDriver->taxiDriverInfo->taxiTypes) ? '':$getDriver->taxiDriverInfo->taxiTypes->waiting_charges,
                        );
if($taxi2!=null) {
             $taxiTypeInfo2=array(
                            'taxi_type' => $taxi2->taxi_type,
                           'taxi_name' => (!$getDriver->taxiDriverInfo->taxiTypes) ? '':$getDriver->taxiDriverInfo->taxiTypes->taxi_name,
                            'base_price' => $taxi2->base_price,
                            'price_per_km' => $taxi2->price_per_km,
                            'taxi_image' => env('APP_URL').'/public/images/taxi/'.$taxi2->taxi_image,
                            'capacity' => (!$getDriver->taxiDriverInfo->taxiTypes) ? '':$getDriver->taxiDriverInfo->taxiTypes->capacity,
                            'waiting_charges' => (!$getDriver->taxiDriverInfo->taxiTypes) ? '':$getDriver->taxiDriverInfo->taxiTypes->waiting_charges,
                        );
         }
         else{
             $taxiTypeInfo2=array();
         }
        }
         else{
           $taxi1=TaxiTypes::where(['id'=>$taxi_driver[0]->fk_taxi_type_id])->first(); 
            $taxiTypeInfo1 =array(
                            'taxi_type' => $taxi1->taxi_type,
                            'taxi_name' => (!$getDriver->taxiDriverInfo->taxiTypes) ? '':$getDriver->taxiDriverInfo->taxiTypes->taxi_name,
                            'base_price' => $taxi1->base_price,
                            'price_per_km' => $taxi1->price_per_km,
                            'taxi_image' => env('APP_URL').'/public/images/taxi/'.$taxi1->taxi_image,
                            'capacity' => (!$getDriver->taxiDriverInfo->taxiTypes) ? '':$getDriver->taxiDriverInfo->taxiTypes->capacity,
                            'waiting_charges' => (!$getDriver->taxiDriverInfo->taxiTypes) ? '':$getDriver->taxiDriverInfo->taxiTypes->waiting_charges,
                        );
            $taxiTypeInfo2=array();
         
         }
                if((!empty($getDriver)))
                {
                    $error = false;
                    
                    if($getDriver->taxiDriverInfo->taxiTypes->taxi_image){
                        $imagePath = env('APP_URL').'/public/images/taxi/'.$getDriver->taxiDriverInfo->taxiTypes->taxi_image;
                    }else{
                        $imagePath = NULL;
                    }


                    if($getDriver->taxiDriverInfo->vehicle_registration_image){
                        $vehicle_registration_image = env('APP_URL').'/public/images/vehicle_registration/'.$getDriver->taxiDriverInfo->vehicle_registration_image;
                    }else{
                        $vehicle_registration_image = NULL;
                    }

                    if($getDriver->driving_licence){
                        $driving_licence_image = env('APP_URL').'/public/images/driver_licence/'.$getDriver->driving_licence;
                    }else{
                        $driving_licence_image = NULL;
                    }


                    if($getDriver->image){
                        $profileImage = env('APP_URL').'/public/images/driver_profile/'.$getDriver->image;
                    }else{
                        $profileImage = NULL;
                    }

                    // $getTotalRides = RideDetails::where(['fk_driver_id' => $driverID,'confirmed' => '1'])->where('ride_status', '!=', 'cancelled')->select(['actual_cost'])->get();

                    $getTotalRides = RideDetails::where(['fk_driver_id' => $driverID])->where('ride_status', '!=', 'cancelled')->select(['actual_cost'])->get();

                    if($getTotalRides){
                            $totalRides = count($getTotalRides);
                                $totalEarnings = 0;
                            foreach($getTotalRides as $getTotalRide){
                                $earning = $getTotalRide->actual_cost;
                                $totalEarnings = round($totalEarnings + $earning, '2');
                            }
                    }else{
                        $totalRides = 0;
                        $totalEarnings = 0;
                    }

                    $data = array(
                        'active' => $getDriver->userDriverInfo->active,
                        'isProfileActive' => $getDriver->userDriverInfo->isProfileActive,
                        'name' => $getDriver->userDriverInfo->name,
                        'user_type' => $getDriver->userDriverInfo->user_type,
                        'email' => $getDriver->userDriverInfo->email,
                        'phone' => $getDriver->userDriverInfo->phone,
                        'driver_id' => $getDriver->fk_user_id,
                        'driver_lat' => $getDriver->driver_lat,
                        'driver_lng' => $getDriver->driver_lng,
                        'profileImage' => $profileImage,
                        'driving_licence_image' => $driving_licence_image,
                        'rating' => $this->getRating($driverID),
                        'addressInfo' => array(
                            'driver_address_id' => (!$getDriver->taxiDriverAddressInfo) ? '':$getDriver->taxiDriverAddressInfo->id,
                            'address' => (!$getDriver->taxiDriverAddressInfo) ? '':$getDriver->taxiDriverAddressInfo->address,
                            'city' => (!$getDriver->taxiDriverAddressInfo) ? '':$getDriver->taxiDriverAddressInfo->city,
                            'state' => (!$getDriver->taxiDriverAddressInfo) ? '':$getDriver->taxiDriverAddressInfo->state,
                            'pincode' => (!$getDriver->taxiDriverAddressInfo) ? '':$getDriver->taxiDriverAddressInfo->pincode,
                        ),
                        'taxiInfo' => array(
                            'taxi_id' => (!$getDriver->taxiDriverInfo) ? '':$getDriver->taxiDriverInfo->id,
                            'taxi_type_id' => (!$getDriver->taxiDriverInfo) ? '':$getDriver->taxiDriverInfo->fk_taxi_type_id,
                            'vehicle_registration_number' => (!$getDriver->taxiDriverInfo) ? '':$getDriver->taxiDriverInfo->vehicle_registration_number,
                            'vehicle_registration_image' => $vehicle_registration_image,
                            'manufacturing_details' => (!$getDriver->taxiDriverInfo) ? '':$getDriver->taxiDriverInfo->manufacturing_details,
                            'tag_number_state' => (!$getDriver->taxiDriverInfo) ? '':$getDriver->taxiDriverInfo->tag_number_state,
                            'insurance_company_information' => (!$getDriver->taxiDriverInfo) ? '':$getDriver->taxiDriverInfo->insurance_company_information,
                        ),
                        'taxiTypeInfo' =>$taxiTypeInfo1,
                        'taxiTypeInfo2'=>$taxiTypeInfo2,

                        'totalRides' => $totalRides,
                        'totalEarnings' => $totalEarnings

                    );
                }else{
                    // $data = (object)array();
                    $data = (object)array();
                }
                    return $data;
    }*/

        protected function loginDriverData($driverID) 
    {
        $getDriver = DriverInfo::where(['fk_user_id' => $driverID])->first();
        $driver_stripe = DriverStripe::where(['user_id'=>$driverID])->first();
        if($driver_stripe != null){
          $stripe_id = $driver_stripe->stripe_id;
          \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

     $a = \Stripe\Account::retrieve(
            $stripe_id
        );
        $capabilities = $a['capabilities'];
       
        }else{
            $capabilities = null;
        }
        $money_split_detail = MoneySplitDetail::where(['driver_id'=>$driverID])->first();
        if($money_split_detail){
        $driver_money = MoneySplitDetail::where(['driver_id'=>$driverID])->sum('driver_money');
        $driver_money=round($driver_money,2);
        $admin_money = MoneySplitDetail::where(['driver_id'=>$driverID])->sum('admin_money');
        $admin_money=round($admin_money,2);
        $total_money = $driver_money+$admin_money;
        $total_money=round($total_money,2);
        }else{
            $driver_money='0';
            $admin_money='0';
            $total_money ='0';
        }
        // echo '<pre>'; print_r($getDriver->userDriverInfo); die();
                 //userDeviceInfo
                 //userDriverInfo
                 //taxiDriverInfo
                 //taxiTypes
                 //taxiDriverAddressInfo
                                
                if((!empty($getDriver)))
                {
                    $error = false;
                    
                    if($getDriver->taxiDriverInfo->taxiTypes->taxi_image){
                        $imagePath = env('APP_URL').'/public/images/taxi/'.$getDriver->taxiDriverInfo->taxiTypes->taxi_image;
                    }else{
                        $imagePath = NULL;
                    }


                    if($getDriver->taxiDriverInfo->vehicle_registration_image){
                        $vehicle_registration_image = env('APP_URL').'/public/images/vehicle_registration/'.$getDriver->taxiDriverInfo->vehicle_registration_image;
                    }else{
                        $vehicle_registration_image = NULL;
                    }

                    if($getDriver->driving_licence){
                        $driving_licence_image = env('APP_URL').'/public/images/driver_licence/'.$getDriver->driving_licence;
                    }else{
                        $driving_licence_image = NULL;
                    }


                    if($getDriver->image){
                        $profileImage = env('APP_URL').'/public/images/driver_profile/'.$getDriver->image;
                    }else{
                        $profileImage = NULL;
                    }

                    // $getTotalRides = RideDetails::where(['fk_driver_id' => $driverID,'confirmed' => '1'])->where('ride_status', '!=', 'cancelled')->select(['actual_cost'])->get();

                    $getTotalRides = RideDetails::where(['fk_driver_id' => $driverID])->where('ride_status', '!=', 'cancelled')->select(['actual_cost'])->get();

                    if($getTotalRides){
                            $totalRides = count($getTotalRides);
                                $totalEarnings = 0;
                            foreach($getTotalRides as $getTotalRide){
                                $earning = $getTotalRide->actual_cost;
                                $totalEarnings = round($totalEarnings + $earning, '2');
                            }
                    }else{
                        $totalRides = 0;
                        $totalEarnings = 0;
                    }

                    $data = array(
                        'active' => $getDriver->userDriverInfo->active,
                        'isProfileActive' => $getDriver->userDriverInfo->isProfileActive,
                        'name' => $getDriver->userDriverInfo->name,
                        'user_type' => $getDriver->userDriverInfo->user_type,
                        'email' => $getDriver->userDriverInfo->email,
                        'phone' => $getDriver->userDriverInfo->phone,
                        'driver_id' => $getDriver->fk_user_id,
                        'driver_lat' => $getDriver->driver_lat,
                        'driver_lng' => $getDriver->driver_lng,
                        'profileImage' => $profileImage,
                        'driving_licence_image' => $driving_licence_image,
                        'rating' => $this->getRating($driverID),
                        'addressInfo' => array(
                            'driver_address_id' => (!$getDriver->taxiDriverAddressInfo) ? '':$getDriver->taxiDriverAddressInfo->id,
                            'address' => (!$getDriver->taxiDriverAddressInfo) ? '':$getDriver->taxiDriverAddressInfo->address,
                            'city' => (!$getDriver->taxiDriverAddressInfo) ? '':$getDriver->taxiDriverAddressInfo->city,
                            'state' => (!$getDriver->taxiDriverAddressInfo) ? '':$getDriver->taxiDriverAddressInfo->state,
                            'pincode' => (!$getDriver->taxiDriverAddressInfo) ? '':$getDriver->taxiDriverAddressInfo->pincode,
                        ),
                        'taxiInfo' => array(
                            'taxi_id' => (!$getDriver->taxiDriverInfo) ? '':$getDriver->taxiDriverInfo->id,
                            'taxi_type_id' => (!$getDriver->taxiDriverInfo) ? '':$getDriver->taxiDriverInfo->fk_taxi_type_id,
                            'vehicle_registration_number' => (!$getDriver->taxiDriverInfo) ? '':$getDriver->taxiDriverInfo->vehicle_registration_number,
                            'vehicle_registration_image' => $vehicle_registration_image,
                            'manufacturing_details' => (!$getDriver->taxiDriverInfo) ? '':$getDriver->taxiDriverInfo->manufacturing_details,
                            'tag_number_state' => (!$getDriver->taxiDriverInfo) ? '':$getDriver->taxiDriverInfo->tag_number_state,
                            'insurance_company_information' => (!$getDriver->taxiDriverInfo) ? '':$getDriver->taxiDriverInfo->insurance_company_information,
                        ),
                        'taxiTypeInfo' =>array(
                            'taxi_type' => (!$getDriver->taxiDriverInfo->taxiTypes) ? '':$getDriver->taxiDriverInfo->taxiTypes->taxi_type,
                           // 'taxi_name' => (!$getDriver->taxiDriverInfo->taxiTypes) ? '':$getDriver->taxiDriverInfo->taxiTypes->taxi_type,
                            'base_price' => (!$getDriver->taxiDriverInfo->taxiTypes) ? '':$getDriver->taxiDriverInfo->taxiTypes->base_price,
                            'price_per_km' => (!$getDriver->taxiDriverInfo->taxiTypes) ? '':$getDriver->taxiDriverInfo->taxiTypes->price_per_km,
                            'taxi_image' => $imagePath,
                            'capacity' => (!$getDriver->taxiDriverInfo->taxiTypes) ? '':$getDriver->taxiDriverInfo->taxiTypes->capacity,
                            'waiting_charges' => (!$getDriver->taxiDriverInfo->taxiTypes) ? '':$getDriver->taxiDriverInfo->taxiTypes->waiting_charges,
                        ),
                        'totalRides' => $totalRides,
                        'totalEarnings' => $total_money,
                        'stripe_id' => $stripe_id,
                        'capabilities' => $capabilities,
                         'driverAmount'=>$driver_money,
                         'adminAmount'=>$admin_money
                    );
                }else{
                    // $data = (object)array();
                    $data = (object)array();
                }
                    return $data;
    }

	public function loginUser(Request $request)
	{

		$error = true;
		$validator = Validator::make($request->all(), [
            'phone' => 'required',
            'user_type' => 'required',
            'deviceId' => 'required',
            'deviceType' => 'required',
            'deviceToken' => 'required',
            'password' => 'required'
        ]);

		if(!$validator->fails()){
			$user = User::where(['phone' => request('phone')])->first();
			if($user){
				if(Hash::check($request->password,$user->password)){
                    if($user->active == '1'){
    					$error = false;

                        if($user->user_type == 'driver'){
                
                        $response = ['message' => 'Login successful.', 'userData' => $this->loginData($user),
                            'driverData' => $this->loginDriverData($user->id)];
                        }else{
                            $response = ['message' => 'Login successful.', 'userData' => $this->loginData($user)];
                        }

                    }else{
                        $response = ['message' => 'Your account is currently Inactive.'];    
                    }

				}else{
				    $response = ['message' => 'Wrong password'];
				}

			} else {
				$response = ['message' => 'User not found.'];
			}
		}
		else {

			$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);

	}

	public function resetPassword(Request $request)
	{
		$error = true;
		$validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'password' => 'required',

        ]);

        if(!$validator->fails()){
			$user = User::where(['id' => request('user_id')])->first();
			if($user){ 
				$user->password = Hash::make($request->password);
            	if($user->save()){
            		$response = ['message' => 'Your password has been successfully reset.'];
            	}
			} else {
				$response = ['message' => 'User not found.'];
			}
		}
		else {

			$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}

	public function registerUser(Request $request)
	{
		$error = true;
	    $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required',

        ]);
		
		if(!$validator->fails()){
			$error = false;
            $user = User::where(['id' => request('user_id'), 'user_type' => 'user'])->first();
	            if($user){
	            	$user->name = $request->name;
	            	$user->email = $request->email;
	            	$user->password = Hash::make($request->password);
	            	$user->newUser = 'no';
	            	if($user->save()){
	            		$response = ['message' => 'Update successfully', 'userData' => $this->loginData($user)];
	            	}

	            }else{
	            	$response = ['message' => 'User not found.'];
	            }
        }else {
				$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}

	public function logout(Request $request)
	{
		$error = true;

        $user = Auth::User();

        if($user){
            $error = false;
            if($user->user_type == 'driver'){
                $userData = User::where(['id' => $user->id])->first();
                if($userData->isProfileActive == '1'){
                    $userData->isProfileActive = '0' ;
                    $userData->save();
                }
            }

            $getUserDeviceToken = UserDeviceInfo::where(['fk_user_id' => $user->id])->get();
            if(isset($getUserDeviceToken)){
                foreach($getUserDeviceToken as $deviceToken){
                    $deviceToken->delete();
                }
            }

            $response = ['message' => 'You are logout successfully.'];
        }else {
                $response = ['message' => 'User not found.'];
        }



		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}


	public function paymentMethodList(Request $request)
	{
		$error = true;
		$user = Auth::User();

		if($user){
			$error = false;
            
            $getCards = UserCards::where(['fk_user_id' => $user->id])->get();

          	if($getCards)
          	{
          		foreach ($getCards as $card) {

          				if($card->card_image){
          					$imagePath = env('APP_URL').'/public/images/user_cards/'.$card->card_image;
          				}else{
          					$imagePath = NULL;
          				}
          			$data[] = array(
          				'card_holder_name' => $card->card_holder_name,
          				'card_type' => $card->card_type,
          				'card_number' => $card->card_number,
          				'cvv' => $card->cvv,
          				'expiry_date' => $card->expiry_date,
          				'card_image' => $imagePath,
          			);
          		}

          		$response = ['message' => 'card list', 'data' => $data];

          	}else{

          		$response = ['message' => 'No card found'];
          	}

        }else {
				
            	$response = ['message' => 'User not found.'];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}

	public function getTaxiTypes(Request $request)
    {
        $error = true;
            if($request->taxi_uses == 'ride'){
            $getTaxis = TaxiTypes::where(['taxi_uses'=>'ride'])->get();
            }
            elseif($request->taxi_uses == 'parcel'){
             $getTaxis = TaxiTypes::where(['taxi_uses'=>'parcel'])->get();    
            }
            else{
              $getTaxis = TaxiTypes::get();   
            }

            if(isset($getTaxis[0]))
            {
                $error = false;
                foreach ($getTaxis as $getTaxi) {

                    if($getTaxi->taxi_image){
                        $imagePath = env('APP_URL').'/public/images/taxi_type/'.$getTaxi->taxi_image;
                        $imagePath = str_replace(' ','%20', $imagePath);
                    }else{
                        $imagePath = NULL;
                    }

                    $data[] = array(
                        'taxi_type_id' => $getTaxi->id,
                        'taxi_type' => $getTaxi->taxi_type,
                       // 'taxi_name' => $getTaxi->taxi_name,
                        'price_per_km' => $getTaxi->price_per_km,
                        'taxi_image' => $imagePath,
                        'taxi_uses' => $getTaxi->taxi_uses,
                        //'capacity' => $getTaxi->capacity,
                        //'waiting_charges' => $getTaxi->waiting_charges,
                        'base_price' => $getTaxi->base_price,
                        'active' => $getTaxi->active,
                        'height' => $getTaxi->height,
                        'width' => $getTaxi->width,
                        'depth' => $getTaxi->depth
                    );
                }

                $response = ['message' => 'Taxi list', 'data' => $data];

            }else{

                $response = ['message' => 'No taxi found'];
            }


        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
    }

	public function updateProfile(Request $request)
	{
		$error = true;
	    $validator = Validator::make($request->all(), [
            'user_type' => 'required',
        ]);
		
		if(!$validator->fails()){
			$error = false;

			$user = Auth::User();
			if($user){

            	$userDetail = User::where(['id' => $user->id])->first();
		            if($userDetail){

                        if((isset($request->deviceToken)) && (isset($request->deviceId)) && (isset($request->deviceId))){
                            $checkUserDevice = UserDeviceInfo::where(['device_id' => $request->deviceId, 'fk_user_id' => $user->id, 'device_type' => $request->deviceType])->first();
                                    if(!$checkUserDevice)
                                    {
                                        $newUserDevice = new UserDeviceInfo();
                                        $newUserDevice->fk_user_id = $user->id;
                                        $newUserDevice->device_type = $request->deviceType;
                                        $newUserDevice->device_token = $request->deviceToken;
                                        $newUserDevice->device_id = $request->deviceId;
                                        $newUserDevice->save();
                                    }else{
                                        $checkUserDevice->device_token = $request->deviceToken;
                                        $currTime = date("Y-m-d H:i:s");
                                        $checkUserDevice->updated_at = $currTime;
                                        $checkUserDevice->save();
                                    }
                        }

		            	$name = (isset($request->name)) ? $request->name : $userDetail->name;
                        $email = (isset($request->email)) ? $request->email : $userDetail->email;
		            	$user_image = (isset($request->user_image)) ? $request->user_image : $userDetail->user_image;

		            	$userDetail->name = $name;
                        $userDetail->email = $email;
		            	$userDetail->user_image = $user_image;
		            	if($request->password){
		            		$userDetail->password = Hash::make($request->password);
		            	}
		            	if($userDetail->save()){
		            		$response = ['message' => 'Update successfully', 'userData' => $this->loginData($userDetail)];
		            	}

		            }else{
		            	$response = ['message' => 'User not found.'];
		            }

	       }else{

				$response = ['message' => 'User not found.'];
			}     

        }else{
				$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}


	public function saveCardDetails(Request $request)
	{
		$error = true;
	    $validator = Validator::make($request->all(), [
            'card_number' => 'required',
            'card_holder_name' => 'required',
            'cvv' => 'required',
            'card_type' => 'required',
        ]);
		
		if(!$validator->fails()){
			$error = false;

			$user = Auth::User();
			if($user){

		        if($request->card_id){

		        	$UserCards = UserCards::where(['id' => $request->card_id])->first();
		        	if($UserCards){

		        		$UserCards->card_type = $request->card_type;
		        		$UserCards->card_number = $request->card_number;
		        		$UserCards->card_holder_name = $request->card_holder_name;
		        		$UserCards->cvv = $request->cvv;
		        		$UserCards->card_type = $request->card_type;
		        		$UserCards->expiry_date = $request->expiry_date;
		        		$UserCards->card_image = $request->card_image;
		        		if($UserCards->save()){
		        			$response = ['message' => 'Card updated successfully.'];
		        		}


		        	}else{
		        		$response = ['message' => 'Card not found'];
		        	}

		        }else{

		        		$checkCard = UserCards::where(['card_number' => $request->card_number])->first();
		        		if($checkCard){
		        			$error = true;
		        			$response = ['message' => 'Card number already exists.'];

		        		}else{

			        		$UserCards = new UserCards();
			        		$UserCards->fk_user_id = $user->id;
			        		$UserCards->card_type = $request->card_type;
			        		$UserCards->card_number = $request->card_number;
			        		$UserCards->card_holder_name = $request->card_holder_name;
			        		$UserCards->cvv = $request->cvv;
			        		$UserCards->expiry_date = $request->expiry_date;
			        		$UserCards->card_image = $request->card_image;
			        		$UserCards->save();

			        		$response = ['message' => 'Card saved successfully'];
						}
		        }

	       }else{

				$response = ['message' => 'User not found.'];
			}     

        }else{
				$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}

	public function getNearByDriversForNotification($current_lat = NULL, $current_lng = NULL, $taxi_type_id = NULL)
	{	

		$lat = (float)$current_lat;
		$lng = (float)$current_lng;
    	$distanceSql = "(3959 * acos(cos(radians(" . $lat . ")) 
            * cos(radians(`driver_lat`)) 
            * cos(radians(`driver_lng`) 
            - radians(" . $lng . ")) 
            + sin(radians(" . $lat . ")) 
            * sin(radians(`driver_lat`))))";
          //distance in miles
          //  $distanceSql= $distanceSql*0.621371;
	    		$getDrivers = DriverInfo::select('id', 'fk_user_id')
				 ->whereHas('taxiDriverInfo',function ($query) use($taxi_type_id){
                        $query->where(['active' => 1, 'fk_taxi_type_id' => (int)$taxi_type_id]);
				  })
                 ->whereHas('userDriverInfo',function ($query){
                        $query->where('isProfileActive', '=', "1");
                  })
                 ->selectRaw("{$distanceSql} AS distance, driver_lat, driver_lng")
                 ->whereRaw("{$distanceSql} < ?", [$this->inMilesRadius])
                 ->get();
      	if((!empty($getDrivers)) && (isset($getDrivers[0])))
      	{
           		foreach ($getDrivers as $drivers) {
  					$data[] =  $drivers->fk_user_id;
      			}
      	}else{
  			 	$data = [];
      	}
        return $data;
	}

   //cronJob function
    public function notifyTime(){
       // Log::error('hi');
    $current_dateTime = date('Y-m-d H:i:s');
    $reachedDetail=RideDetails::where(['ride_status'=>'reached','is_waiting_notified'=>0])->get();
    foreach($reachedDetail as $reachedUser){
        $from = Carbon::parse($reachedUser->reached_time);
        $to = Carbon::parse($current_dateTime);
        $diff_in_minutes = $to->diffInMinutes($from);
        $time=WaitingCharges::first();
             if($diff_in_minutes>=$time->time_after_charge){
                $getDriverName = User::select(['name'])->where(['id' =>$reachedUser->fk_driver_id])->first();
                if($getDriverName){
                    $extraPara = ['ride_id' => $reachedUser->id, 'driverName' => $getDriverName->name];
                }
                else{
                   $extraPara=null;
                }
                 
                $requestNotification = $this->sendNotification('waiting-time-charge',$reachedUser->fk_user_id,$extraPara);
                $new=RideDetails::find($reachedUser->id);
                $new->is_waiting_notified=1;
                $new->save();
             }

       }

    }



	public function createRide(Request $request)
    {
        $req = json_encode($_POST);
        // mail('vasu.bansal@mail.vinove.com','subject',$req);
        $error = true;
        $validator = Validator::make($request->all(), [
            // 'ride_type' => 'required',
            // 'ride_status' => 'required',
        ]);
		
		if(!$validator->fails()){
			$error = false;

			$user = Auth::User();
			if($user){
                
		        if($request->ride_id){
                    
		        	$ride = RideDetails::where(['id' => $request->ride_id])->first();
		        	if($ride){
		        		if($user->user_type == 'user'){
                            $ride->fk_user_id = $user->id;
                        }
		        		//$ride->fk_driver_id = $request->driver_id;
		        		$ride->fk_taxi_id = (isset($request->taxi_type_id)) ? $request->taxi_type_id : $ride->fk_taxi_id;
		        		$ride->ride_type = (isset($request->ride_type)) ? $request->ride_type : $ride->ride_type;
		        		$ride->ride_status = (isset($request->ride_status)) ? $request->ride_status : $ride->ride_status;
		        		$ride->pickup_address = (isset($request->pickup_address)) ? $request->pickup_address : $ride->pickup_address;
		        		$ride->drop_address = (isset($request->drop_address)) ? $request->drop_address : $ride->drop_address;
		        		$ride->pickup_lat = (isset($request->pickup_lat)) ? $request->pickup_lat : $ride->pickup_lat;
		        		$ride->pickup_lng = (isset($request->pickup_lng)) ? $request->pickup_lng : $ride->pickup_lng;
		        		$ride->drop_lat = (isset($request->drop_lat)) ? $request->drop_lat : $ride->drop_lat;
		        		$ride->drop_lng = (isset($request->drop_lng)) ? $request->drop_lng : $ride->drop_lng;
		        		$ride->actual_cost = (isset($request->actual_cost)) ? $request->actual_cost : $ride->actual_cost;
		        		$ride->pay_by = (isset($request->pay_by)) ? $request->pay_by : $ride->pay_by;
		        		$ride->parcel_name = (isset($request->parcel_name)) ? $request->parcel_name : $ride->parcel_name;
                        $ride->parcel_height = (isset($request->parcel_height)) ? $request->parcel_height : $ride->parcel_height;
		        		$ride->parcel_width = (isset($request->parcel_width)) ? $request->parcel_width : $ride->parcel_width;
                        $ride->parcel_depth = (isset($request->parcel_depth)) ? $request->parcel_depth : $ride->parcel_depth;
		        		$ride->parcel_weight = (isset($request->parcel_weight)) ? $request->parcel_weight : $ride->parcel_weight;
                        if($request->cancel){

		        		$ride->cancel = (isset($request->cancel)) ? $request->cancel : $ride->cancel;
                        $ride->cancelled_at = date('Y-m-d H:i:s');
                        }
		        		$ride->cancel_reason = (isset($request->cancel_reason)) ? urldecode ($request->cancel_reason) : $ride->cancel_reason;
                        $ride->cancel_type = (isset($request->cancel_type)) ? $request->cancel_type : $ride->cancel_type;
                       // $ride->waiting_time = (isset($request->waiting_time)) ? $request->waiting_time : $ride->waiting_time;
                        if($request->ride_status == 'begin'){ 
                            $current_dateTime = date('Y-m-d H:i:s');
                            $ride->pickup_time = $current_dateTime;
                            $from = Carbon::parse($ride->reached_time);
                            $to = Carbon::parse($current_dateTime);
                            $diff_in_minutes = $to->diffInMinutes($from);
                            $ride->waiting_time = $diff_in_minutes;
                           
                           /* $waitingCharges = WaitingCharges::where(['id'=>1])->first();
                             if(isset($waitingCharges->time_after_charge) && ($diff_in_minutes > $waitingCharges->time_after_charge)){
                            $waiting_charge = ($diff_in_minutes - $waitingCharges->time_after_charge);
                            $waiting_charge =  ($waiting_charge * $waitingCharges->charges_per_min)/100;
                            $waiting_charge = round($waiting_charge,2);
                            $ride->waiting_charge = $waiting_charge;
                        }*/
                        }
                        if($request->ride_status == 'end'){
                            // $date = new DateTime('now');
                            // $date->setTimezone(new DateTimeZone('Asia/Calcutta'));
                            // $current_dateTime = $date->format('Y-m-d H:i:s');
                            $current_dateTime = date('Y-m-d H:i:s');
                            $ride->drop_time = $current_dateTime;
                        }
                        if($request->ride_status == 'reached'){
                          $current_dateTime = date('Y-m-d H:i:s');
                          $ride->reached_time = $current_dateTime;
                        }
                       
                       if(isset($request->isModify) && ($request->isModify == 1)){
                         if(isset($request->distance) && isset($request->taxi_type_id)){  +-

                            $taxi_info = TaxiTypes::where(['id' => $request->taxi_type_id])->first();
                            if($taxi_info)
                            {
                                $estimated_cost = $request->distance * $taxi_info->price_per_km; 
                                $estimated_cost = $estimated_cost + $taxi_info->base_price;
                            }
                           
                           $ride->estimated_cost = $estimated_cost;
                           $ride->is_modified = 1;
                           } 
                          else{
                             $error=true;
                             $response = ['message' => 'distance or taxi_type_id not found'];
                             }
                       }    
                         


                        $ride->comments = (isset($request->comments)) ? $request->comments : $ride->comments;
                        $ride->tips = (isset($request->tips)) ? $request->tips : $ride->tips;
                        if($request->ride_status == 'user_expire'){
                            $getNotifications = Notifications::where(['templateName' =>  'driver-ride-request'])->where('extra', 'LIKE', '%"ride_id":'.$request->ride_id.'%')->get();

                            if(isset($getNotifications)){
                                foreach($getNotifications as $notifications){
                                    $currTime = date("Y-m-d H:i:s");
                                    $notifications->deleted_at = $currTime;
                                    $notifications->save();
                                    // $notifications->delete();
                                }
                            }
                        }
                        if($request->ride_status == 'end'){
                            $rideData = RideLatLng::where(['fk_ride_id' => $request->ride_id])->get();
                            $countrecords = count($rideData);
                            if(isset($rideData[0])){
                                $distance = 0;
                                for($i=0; $i<$countrecords-1; $i++){
                                    $distance += $this->distanceCalculation($rideData[$i]->latitude, $rideData[$i]->longitude, $rideData[$i+1]->latitude, $rideData[$i+1]->longitude);
                                }
                                $finalDistance = $distance * 69.05482;
                                $finalDistance = round($finalDistance, '2');

                                if($finalDistance < 1){
                                     $finalDistance = 1;
                                }

                                $ride->distance = $finalDistance;
                                $getPerKmRate = TaxiTypes::where(['id' => $ride->fk_taxi_id])->first();
                                $waitingCharges = WaitingCharges::first();

                                if(isset($getPerKmRate)){
                                    $price = $finalDistance * $ride->taxi_price;
                                    $priceWithBasePrice = $price + $ride->taxi_base_price;

                                    $rideWaiting=($ride->waiting_time);
                                   
                                if(isset($waitingCharges->time_after_charge) && ($rideWaiting > $waitingCharges->time_after_charge)){
                                  $waiting_charge = ($rideWaiting - $waitingCharges->time_after_charge);
                                  $waiting_charge =  ($waiting_charge * $waitingCharges->charges_per_min)/100;
                                  $waiting_charge = round($waiting_charge,2);
                                  $priceWithWaiting = $priceWithBasePrice + $waiting_charge;
                                }
                                else{
                                     $waiting_charge = 0;
                                     $priceWithWaiting = $priceWithBasePrice;
                                }
                
                                 //  if()
                                   // $priceWithWaiting = $priceWithBasePrice;
                                    /*if($ride->waiting_time){
                                        $priceWithWaiting = $ride->waiting_time/60 * $getPerKmRate->waiting_charges + $priceWithBasePrice;
                                    }else{
                                        $priceWithWaiting = $priceWithBasePrice;
                                    }

*/

                                    if(isset($getPerKmRate->estimated_tax)){
                                        $calculateTax = $priceWithWaiting * ($getPerKmRate->estimated_tax/ 100);
                                        $calculateTax = round($calculateTax, 2);
                                        $finalPrice = $priceWithWaiting + $calculateTax;
                                    }else{
                                        $finalPrice = $priceWithWaiting;
                                        $calculateTax = 0;
                                    }

                                    /* $commission = Commission::where(['id' => 1])->first();
                                        if($commission){
                                         $driverFare = $finalPrice*($commission->driver_commission / 100);
                                         $adminFare =  $finalPrice*($commission->admin_commission / 100);
                                          }*/


                                    if($ride->tips){
                                        $finalPrice=$ride->tips+ $finalPrice;
                                    }
                                

                                }else{
                                    $finalPrice = $finalDistance * 1;
                                    $priceWithWaiting = $finalPrice;
                                    $calculateTax = 0;
                                }

                            }else{
                                 
                              
                                $getPerKmRate = TaxiTypes::where(['id' => $ride->fk_taxi_id])->first();
                                $waitingCharges = WaitingCharges::first();
                                $finalDistance = 1;
                                if(isset($getPerKmRate)){
                                   /* $price = $finalDistance * $getPerKmRate->price_per_km;
                                    $priceWithBasePrice = $price + $getPerKmRate->base_price;*/
                                    $price = $finalDistance * $ride->taxi_price;
                                    $priceWithBasePrice = $price + $ride->taxi_base_price;
                                    $priceWithWaiting =  $priceWithBasePrice;

                                 $rideWaiting=($ride->waiting_time);
                                  
                                if(isset($waitingCharges->time_after_charge) && ($rideWaiting >$waitingCharges->time_after_charge)){
                                  $waiting_charge = ($rideWaiting - $waitingCharges->time_after_charge);
                                  $waiting_charge = ($waiting_charge * $waitingCharges->charges_per_min)/100;
                                   $waiting_charge = round($waiting_charge,2);
                                  $priceWithWaiting = $priceWithBasePrice + $waiting_charge;
                                }
                                else{
                                     $waiting_charge = 0;
                                     $priceWithWaiting = $priceWithBasePrice;
                                }

                                   /* if($ride->waiting_time){
                                        $priceWithWaiting = $ride->waiting_time/60 * $getPerKmRate->waiting_charges + $priceWithBasePrice;
                                    }else{
                                        $priceWithWaiting = $priceWithBasePrice;
                                    }*/

                                    if(isset($getPerKmRate->estimated_tax)){
                                        $calculateTax = $priceWithWaiting * ($getPerKmRate->estimated_tax/ 100);
                                        $calculateTax = round($calculateTax, 2);
                                        $finalPrice = $priceWithWaiting + $calculateTax;

                                    }else{
                                        $finalPrice = $priceWithWaiting;
                                        $calculateTax = 0;
                                    }
                                    /* $commission = Commission::where(['id' => 1])->first();
                                        if($commission){
                                         $driverFare = $finalPrice*($commission->driver_commission / 100);
                                         $adminFare =  $finalPrice*($commission->admin_commission / 100);
                                          }
*/


                                    if($ride->tips){
                                        $finalPrice=$ride->tips+ $finalPrice;
                                    }

                                  
                                }else{
                                    $finalPrice = 0;
                                    $priceWithWaiting = 0;
                                    $calculateTax = 0;
                                    
                                }
                            }
                            $ride->waiting_charge =  $waiting_charge ;                      
                            $ride->actual_cost = round($finalPrice,2);
                            $ride->ride_fare = round($priceWithBasePrice, 2);
                            $ride->estimated_tax = round($calculateTax, 2);
                        }else{
                            $finalPrice = $ride->actual_cost;
                            $priceWithBasePrice =  $ride->ride_fare;
                            $calculateTax = $ride->estimated_tax;
                            $waiting_charge=0;
                        }

		        		if($ride->save())
                        {
		        			if($request->ride_status == 'arriving'){

                                $getDriverName = User::select(['name'])->where(['id' => $user->id])->first();
                                $extraPara = ['ride_id' => $request->ride_id, 'driverName' => $getDriverName->name];
                                $requestNotification = $this->sendNotification('driver-arriving-to-user', $ride->fk_user_id, $extraPara);

                            }elseif($request->ride_status == 'reached'){
                                $waitingCharges = WaitingCharges::first();

                                $getDriverName = User::select(['name'])->where(['id' => $user->id])->first();
                                $extraPara = ['ride_id' => $request->ride_id, 'driverName' => $getDriverName->name, 'waitingTime' => $waitingCharges->time_after_charge];
                                $requestNotification = $this->sendNotification('driver-reached-to-user', $ride->fk_user_id, $extraPara);

                            }elseif($request->ride_status == 'begin'){

                                $getDriverName = User::select(['name'])->where(['id' => $user->id])->first();
                                $extraPara = ['ride_id' => $request->ride_id, 'driverName' => $getDriverName->name];
                                $requestNotification = $this->sendNotification('driver-start-for-parcel', $ride->fk_user_id, $extraPara);

                            }elseif($request->ride_status == 'end'){

                                $getDriverName = User::select(['name'])->where(['id' => $user->id])->first();
                                $extraPara = ['ride_id' => $request->ride_id, 'driverName' => $getDriverName->name];
                                $requestNotification = $this->sendNotification('driver-end-for-parcel', $ride->fk_user_id, $extraPara);

                            }elseif(($request->ride_status == 'cancelled') && ($request->cancel_type == 'user')){
                                $extraPara = ['ride_id' => $request->ride_id];
                                $requestNotification = $this->sendNotification('user-cancelled-ride', $ride->fk_driver_id, $extraPara);

                            }elseif(($request->ride_status == 'cancelled') && ($request->cancel_type == 'driver')){

                                $getDriverName = User::select(['name'])->where(['id' => $user->id])->first();
                                $extraPara = ['ride_id' => $request->ride_id, 'driverName' => $getDriverName->name];
                                $requestNotification = $this->sendNotification('driver-cancelled-ride', $ride->fk_user_id, $extraPara);
                            }
                            
                            if(isset($request->isModify) && ($request->isModify == 1)){
                                // Log::info(['hi']);
                                $getDriverName = User::select(['name'])->where(['id' => $user->id])->first();
                                $extraPara = ['ride_id' => $request->ride_id, 'driverName' => $getDriverName->name];
                                $requestNotification = $this->sendNotification('user-update-location', $ride->fk_driver_id, $extraPara);

                            }
                            $getTaxiType = TaxiTypes::where(['id' => $ride->fk_taxi_id])->first();
                            if(isset($getTaxiType)){
                                $tax = $getTaxiType->estimated_tax;
                            }else{
                                $tax = NULL;
                            }

                            if($ride->tips){
                            $tips=$ride->tips;
                            }
                            else{
                            $tips =null ;  
                            }
                            //commission

                         
                           
                            if($error == false){
                                $response = ['message' => 'Ride updated successfully.',
                                'rideData' => $this->rideData($ride->id),
                                'userData' => $this->userData($ride->fk_user_id), 
                                'driverData' => $this->driverData($ride->fk_driver_id,$ride->fk_user_id),
                                'taxiData' => $this->taxiData($ride->fk_taxi_id),
                                'fareData' => array('ride_fare' => $priceWithBasePrice,
                                                    'waiting_charge' =>$waiting_charge,
                                                    //'tax' => $tax,
                                                    'tips' =>$tips,
                                                    'estimated_tax' => round($calculateTax,2),
                                                    'total_bill' => round($finalPrice,2)
                                                   ),
                                /*'Share' => array( 'driver_fare' =>$driverFare,
                                                   'admin_fare' =>$adminFare
                                )*/
                                ];
                            }
		        		}else{
                            $response = ['message' => 'Ride not found'];
                        }

		        	}else{
		        		$response = ['message' => 'Ride not found'];
		        	}

		        }else{
                       
                        if(isset($request->fk_driver_id)){
                            
                            $checkDriverAvailability = User::select(['isProfileActive'])->where(['id' => $request->fk_driver_id])->first();

                            if(isset($checkDriverAvailability)){
                                if($checkDriverAvailability->isProfileActive == '0'){
                                    $response = ['message' => 'Driver is not available.'];
                                    $response['code'] = $this->errorCode;

                                    return response()->json(['response' => $response], $response['code']);
                                }
                            }
                        }

		        		$estimated_cost = "";
		        		if($request->taxi_type_id)
		        		{
                            
		        			$taxi_info = TaxiTypes::where(['id' => $request->taxi_type_id])->first();
		        			if($taxi_info)
		        			{
		        				$estimated_cost = $request->distance * $taxi_info->price_per_km; 
                                $estimated_cost = $estimated_cost + $taxi_info->base_price;
                                $taxi_price = $taxi_info->price_per_km;
                                $taxi_base_price = $taxi_info->base_price;
		        			}
		        		}
                          
		        		if(empty($estimated_cost))
		        		{
		        			$estimated_cost = "50";
		        		}

		        		$ride = new RideDetails();
		        		$ride->fk_user_id = $user->id;
                        // $ride->fk_driver_id = '1';
                        if(isset($request->fk_driver_id)){
                            $ride->fk_driver_id = $request->fk_driver_id;    
                        }
		        		$ride->fk_taxi_id = $request->taxi_type_id;
		        		$ride->ride_type = $request->ride_type;
		        		// $ride->ride_status = $request->ride_status;
                        $ride->ride_status = 'new';
		        		$ride->pickup_address = $request->pickup_address;
		        		$ride->drop_address = $request->drop_address;
		        		$ride->pickup_lat = $request->pickup_lat;
		        		$ride->pickup_lng = $request->pickup_lng;
		        		$ride->drop_lat = $request->drop_lat;
		        		$ride->drop_lng = $request->drop_lng;
		        		$ride->estimated_cost = $estimated_cost;
		        		$ride->actual_cost = $request->actual_cost;
		        		$ride->pay_by = $request->pay_by;
                        $ride->parcel_name = $request->parcel_name;
		        		$ride->parcel_height = $request->parcel_height;
		        		$ride->parcel_width = $request->parcel_width;
                        $ride->parcel_depth = $request->parcel_depth;
		        		$ride->parcel_weight = $request->parcel_weight;
		        		$ride->cancel = $request->cancel;
		        		$ride->cancel_reason = $request->cancel_reason;
                        $ride->pickup_time = $request->pickup_time;
                        $ride->drop_time = $request->drop_time;
                        $ride->comments = $request->comments;
                        $ride->tips = $request->tips;
                        $ride->booking_through = $request->booking_through;
                        $ride->taxi_price =$taxi_price;
                        $ride->taxi_base_price =  $taxi_base_price;
		        		$ride->save();
                    
                            $response = ['message' => 'Ride created successfully.',
                            'rideData' => $this->rideData($ride->id),
                            'userData' => $this->userData($ride->fk_user_id), 
                            'driverData' => $this->driverData($ride->fk_driver_id, $ride->fk_user_id),
                            'taxiData' => $this->taxiData($ride->fk_taxi_id),
                            ];

                            if(isset($request->fk_driver_id)){

                                $extraPara = ['from' => $request->pickup_address, 'to' => $request->drop_address,'ride_id' => $ride->id];
                                        $requestNotification = $this->sendNotification('driver-ride-request', $request->fk_driver_id, $extraPara);

                            }else{
                                //code for send and saved the notifications
                                $getNearByDrivers = $this->getNearByDriversForNotification($request->pickup_lat,$request->pickup_lng, $request->taxi_type_id);
                                

                                // print_r($getNearByDrivers); die();
                                if(isset($getNearByDrivers[0])){
                                    foreach($getNearByDrivers as $nearByDriverID)
                                    {
                                        
                                        $extraPara = ['from' => $request->pickup_address, 'to' => $request->drop_address,'ride_id' => $ride->id];
                                        $requestNotification = $this->sendNotification('driver-ride-request', $nearByDriverID, $extraPara);
                                }
                                }
                            }

                            
		        }

	       }else{

				$response = ['message' => 'User not found.'];
			}     

        }else{
				$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	
}
	public function updateRideLatLng(Request $request)
	{
		$error = true;
	    $validator = Validator::make($request->all(), [
            'ride_id' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);
		
		if(!$validator->fails()){
			$error = false;

			$user = Auth::User();
			if($user){

		        	$ride = RideDetails::where(['id' => $request->ride_id])->first();
		        	if($ride){

		        			$rideLatLng = new RideLatLng();
		        			$rideLatLng->fk_ride_id = $request->ride_id;
		        			$rideLatLng->latitude = $request->latitude;
		        			$rideLatLng->longitude = $request->longitude;
		        			if($rideLatLng->save()){
		        				$error = false;
		        				$response = ['message' => 'Ride Lat Lng has been inserted successfully.'];
		        			}else{
		        				$response = ['message' => 'Some problem occured.'];
		        			}
		        	
		        	}else{
		        		$response = ['message' => 'Ride not found'];
		        	}
	       }else{
					$response = ['message' => 'User not found.'];
			}     

        }else{
				$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}

	protected function rideData($rideID) 
	{
		 $rideModel = RideDetails::where(['id' => $rideID])->first();

         if($rideModel){
            $get_driver_rating = DriverRating::where(['fk_driver_id' => $rideModel->fk_driver_id, 'fk_user_id' => $rideModel->fk_user_id,'fk_ride_id' => $rideModel->id])->first();
            if($get_driver_rating){
                $user_rating = $get_driver_rating->rating;
            }else{
                $user_rating = 0;
            }


         $check_driver_rating_to_user = UserRating::where(['fk_driver_id' => $rideModel->fk_driver_id, 'fk_user_id' => $rideModel->fk_user_id,'fk_ride_id' => $rideModel->id])->first();
         if($check_driver_rating_to_user){
            $check_driver_rating = 1;
         }else{
            $check_driver_rating = 0;
         }
		 	return [
				'ride_id' => $rideModel->id,
				'ride_type' => $rideModel->ride_type,
				'ride_status' => $rideModel->ride_status,
				'confirmed' => $rideModel->confirmed,
				'pickup_address' => $rideModel->pickup_address,
				'drop_address' => $rideModel->drop_address,
				'pickup_lat' => $rideModel->pickup_lat,
				'pickup_lng' => $rideModel->pickup_lng,
				'drop_lat' => $rideModel->drop_lat,
				'drop_lng' => $rideModel->drop_lng,
                'pickup_time' => $rideModel->pickup_time,
                'drop_time' => $rideModel->drop_time,
                'comments' => $rideModel->comments,
                'tips' => $rideModel->tips,
				'estimated_cost' => $rideModel->estimated_cost,
                'actual_cost' => $rideModel->actual_cost,
                'estimated_tax' => $rideModel->estimated_tax,
				'ride_fare' => $rideModel->ride_fare,
				'pay_by' => $rideModel->pay_by,
                'parcel_name' => $rideModel->parcel_name, 
				'parcel_height' => $rideModel->parcel_height,
				'parcel_width' => $rideModel->parcel_width,
                'parcel_depth' => $rideModel->parcel_depth,
				'parcel_weight' => $rideModel->parcel_weight,			
				'cancel' => $rideModel->cancel,			
                'cancel_reason' => $rideModel->cancel_reason,           
				'cancel_type' => $rideModel->cancel_type,			
				'advanceRide' => $rideModel->advanceRide,
                'rating' => $this->getRating($rideModel->fk_driver_id),
                'user_rating'=>  $user_rating, 
                'distance' => $rideModel->distance,
                'payment_status' => $rideModel->payment_status,
                'booking_through' => $rideModel->booking_through,
                'check_driver_rating' => $check_driver_rating,
			];

		 }else{
		 	return (object)array();
		 }
	}

    protected function getRating($driverID){
        $getRating = DriverRating::where(['fk_driver_id' => $driverID])->avg('rating');
        if($getRating){
            $rating = number_format($getRating, 1, '.', '');
        }else{
            $rating = 0.0;
        }
        return $rating;
    }

	protected function userData($userID) 
	{
		 //echo '<pre>'; print_r($rideModel); die();
         $user_stripe = UserStripe::where(['user_id'=>$userID])->first();
         if($user_stripe){
            
         \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
         $a =\Stripe\PaymentMethod::all([
          'customer' => $user_stripe->customer_id,
          'type' => 'card',
        ]);
          
       if ($a['data'] == null){
          $card_detail = '0';
       }else{
           $card_detail = '1';
       }
            
         }else{
            $card_detail = '0';
         }
         $getRating = UserRating::where(['fk_user_id' => $userID])->avg('rating');
                    if($getRating){
                        $rating = number_format($getRating, 1, '.', '');
                    }else{
                        $rating = 0.0;
                    }

		 $userModel = User::where(['id' => $userID, 'user_type' => 'user'])->first();
         $getWallet = UserWallet::where(['fk_user_id' => $userID])->first();
            if($getWallet)
            {
                $userWallet = $getWallet->wallet_amount;
            }else{
                $userWallet = 0;
            }
		 if($userModel){
            if($userModel->user_image){
                $profileImage = env('APP_URL').'/public/images/user_profile/'.$userModel->user_image;
            }else{
                $profileImage = NULL;
            }


		 	return [
				'user_id' => $userModel->id,
                'active' => $userModel->active,
				'name' => $userModel->name,
                'user_type' => $userModel->user_type,
				'email' => $userModel->email,
				'phone' => $userModel->phone,
                'wallet_amount' => $userWallet,
                'user_image' => $profileImage,
                'rating'=>$rating,
                'card_detail'=>$card_detail
			];
		 }else{
		 	return (object)array();
		 }
	}

	protected function driverData($driverID, $userID = NULL) 
	{
		 $driverModel = User::where(['id' => $driverID, 'user_type' => 'driver'])->first();
         $getDriver = DriverInfo::where(['fk_user_id' => $driverID])->first();

         $check_favourite = UserFavouriteDriver::where(['fk_driver_id' => $driverID,'fk_user_id' => $userID, 'favourite' => '1'])->first();
         if($check_favourite){
            $favourite = '1';
         }else{
            $favourite = '0';
         }
		 //echo '<pre>'; print_r($driverModel->userDriverInfo); die();
        if(isset($getDriver->image)){
            $profileImage = env('APP_URL').'/public/images/driver_profile/'.$getDriver->image;
        }else{
            $profileImage = NULL;
        }

        if(isset($getDriver->driving_licence)){
            $driving_licence_image = env('APP_URL').'/public/images/driver_licence/'.$getDriver->driving_licence;
        }else{
            $driving_licence_image = NULL;
        }

		 if($driverModel){
		 	return [
				'driver_id' => $driverModel->id,
				'driver_lat' => $driverModel->userDriverInfo->driver_lat,
				'driver_lng' => $driverModel->userDriverInfo->driver_lng,
                'profileImage' => $profileImage,
                'driving_licence_image' => $driving_licence_image,
                'name' => $driverModel->name,
                'email' => $driverModel->email,
                'phone' => $driverModel->phone,
                'rating' => $this->getRating($driverID),
                'favourite' => $favourite,
                    'addressInfo' => array(
                            'driver_address_id' => (!$getDriver->taxiDriverAddressInfo) ? '':$getDriver->taxiDriverAddressInfo->id,
                            'address' => (!$getDriver->taxiDriverAddressInfo) ? '':$getDriver->taxiDriverAddressInfo->address,
                            'city' => (!$getDriver->taxiDriverAddressInfo) ? '':$getDriver->taxiDriverAddressInfo->city,
                            'state' => (!$getDriver->taxiDriverAddressInfo) ? '':$getDriver->taxiDriverAddressInfo->state,
                            'pincode' => (!$getDriver->taxiDriverAddressInfo) ? '':$getDriver->taxiDriverAddressInfo->pincode,
                        )
			];
		 }else{
		 	return (object)array();
		 }
	}

	protected function taxiData($taxiID,$driverID = NULL) 
	{
        
		if(isset($driverID)){

            $taxiModelCar = TaxiDriverInfo::where(['fk_user_id' => $driverID, 'fk_taxi_type_id' => $taxiID])->first();
        }
        
        $taxi_id = (isset($taxiModelCar->id)) ? $taxiModelCar->id : NULL; 
        $active = (isset($taxiModelCar->active)) ? $taxiModelCar->active : NULL; 
        $vehicle_registration_number = (isset($taxiModelCar->vehicle_registration_number)) ? $taxiModelCar->vehicle_registration_number : NULL; 
        $manufacturing_details = (isset($taxiModelCar->manufacturing_details)) ? $taxiModelCar->manufacturing_details : NULL; 
        $tag_number_state = (isset($taxiModelCar->tag_number_state)) ? $taxiModelCar->tag_number_state : NULL; 
        $insurance_company_information = (isset($taxiModelCar->insurance_company_information)) ? $taxiModelCar->insurance_company_information : NULL; 


        $taxiModel = TaxiTypes::where(['id' => $taxiID])->first();

         if($taxiModel->taxi_image){
            $imagePath = env('APP_URL').'/public/images/taxi/'.$taxiModel->taxi_image;
        }else{
            $imagePath = NULL;
        }


        if(isset($taxiModelCar->vehicle_registration_image)){
            $vehicle_registration_image = env('APP_URL').'/public/images/vehicle_registration/'.$taxiModelCar->vehicle_registration_image;
        }else{
            $vehicle_registration_image = NULL;
        }

		 // echo '	<pre>'; print_r($taxiModelCar->taxiTypes); die();
		 if($taxiModel){
		 	return [
				'taxi_id' => $taxi_id,
				'active' => $active,
				'vehicle_registration_number' => $vehicle_registration_number,
				'vehicle_registration_image' => $vehicle_registration_image,
				'manufacturing_details' => $manufacturing_details,
				'tag_number_state' => $tag_number_state,
				'insurance_company_information' => $insurance_company_information,
				'taxiTypeInfo' => [
					'taxi_type_id' => $taxiModel->id,
					'active' => $taxiModel->active,
					//'taxi_name' => $taxiModel->taxi_name,
					'base_price' => $taxiModel->base_price,
                    'price_per_km' => $taxiModel->price_per_km,
                    'taxi_image' => $imagePath,
                   // 'capacity' => $taxiModel->capacity,
                   // 'waiting_charges' => $taxiModel->waiting_charges,
				]

			];
		 }else{
		 	return (object)array();
		 }
	}

    public function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long) {
        
        if((isset($point1_lat)) && (isset($point1_long)) && (isset($point2_lat)) && (isset($point2_long))){

        // Calculate the distance in degrees
           $distance = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));
         //distance in miles
          // $distance = $distance*0.621371;
            return $distance;
        }else{
            return 0;
        }
    }


	public function getRideStatus(Request $request)
	{

        // $rideData = RideLatLng::where(['fk_ride_id' => '5'])->get();
        // $countrecords = count($rideData);
        // $distance = 0;
        // for($i=0; $i<$countrecords-1; $i++)
        // {
        //     $distance += $this->distanceCalculation($rideData[$i]->latitude, $rideData[$i]->longitude, $rideData[$i+1]->latitude, $rideData[$i+1]->longitude);
        // }
        // $finalDistance = $distance * 111.13384;
        // $finalDistance = round($finalDistance, '2');
        
        // $getPerKmRate = TaxiTypes::where(['id' => '1'])->first();
        // if(isset($getPerKmRate)){
        //     $price = $finalDistance * $getPerKmRate->price_per_km;
        // }else{
        //     $price = $finalDistance * 10;
        // }
        //     $priceWithBasePrice = $price + $getPerKmRate->base_price;
        //     $priceWithWaiting = 120/60 * $getPerKmRate->waiting_charges + $priceWithBasePrice;
        // echo $priceWithWaiting;
        // die();

		$error = true;
	    $validator = Validator::make($request->all(), [
            'ride_id' => 'required',
        ]);
		
		if(!$validator->fails()){
			$error = false;

			$ride = RideDetails::where(['id' => $request->ride_id])->first();
			if($ride){
                $getTaxiType = TaxiTypes::where(['id' => $ride->fk_taxi_id])->first();
                    if(isset($getTaxiType)){
                        $tax = $getTaxiType->estimated_tax;
                    }else{
                        $tax = NULL;
                    }
                    


				$response = ['message' => 'All ride data',
							'rideData' => $this->rideData($ride->id),
							'userData' => $this->userData($ride->fk_user_id), 
							'driverData' => $this->driverData($ride->fk_driver_id,$ride->fk_user_id),
							'taxiData' => $this->taxiData($ride->fk_taxi_id, $ride->fk_driver_id),
                            'fareData' => array('ride_fare' => $ride->ride_fare,
                                                'waiting_charge'=>$ride->waiting_charge,
                                                'tips'=>$ride->tips,
                                                //'tax' => $tax,
                                                'tax' => $ride->estimated_tax,
                                                'total_bill' => $ride->actual_cost,
                                                'isModify' => $ride->is_modified
                                               )
							];
       }else{

				$response = ['message' => 'Ride not found.'];
			}     

        }else{
				$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}

  

	public function getNearByDrivers(Request $request)
	{
		$error = true;
		$validator = Validator::make($request->all(), [
            'current_lat' => 'required',
            'current_lng' => 'required',
            'inKmRadius' => 'required',
        ]);
		    
			if(!$validator->fails()){
			$error = false;
            	
				$lat = (float)$request->current_lat;
				$lng = (float)$request->current_lng;
				$inMilesRadius = $request->inKmRadius;
                $limit = $this->limit;

            	$distanceSql = "(3959 * acos(cos(radians(" . $lat . ")) 
                    * cos(radians(`driver_lat`)) 
                    * cos(radians(`driver_lng`) 
                    - radians(" . $lng . ")) 
                    + sin(radians(" . $lat . ")) 
                    * sin(radians(`driver_lat`))))";
                    //distance in Miles
                    // $distanceSql=$distanceSql*0.621371;
                     //dd($distanceSql);
                 	if($request->taxi_type_id){
                        $taxi_type_id = $request->taxi_type_id;
                        $getDrivers = DriverInfo::select('id', 'fk_user_id')
                          ->whereHas('taxiDriverInfo',function ($query) use($taxi_type_id){
                            $query->where(['active' => 1, 'fk_taxi_type_id' => (int)$taxi_type_id]);
                          })
                         ->whereHas('userDriverInfo',function ($query){
                                $query->where('isProfileActive', '=', "1");
                          })
                         ->selectRaw("{$distanceSql} AS distance, driver_lat, driver_lng")
                         ->whereRaw("{$distanceSql} < ?", [$inMilesRadius])
                         ->offset(0)
                         ->limit($limit)
                         ->orderBy('distance', 'ASC')
                         ->get();
                         
			    	}else{
			    		$getDrivers = DriverInfo::select('id', 'fk_user_id')
						 ->whereHas('taxiDriverInfo',function ($query) {
							    $query->where('active', '=', "1");
						  })
		                 ->selectRaw("{$distanceSql} AS distance, driver_lat, driver_lng")
                         ->whereRaw("{$distanceSql} < ?", [$inMilesRadius])
		                 ->whereRaw("{$distanceSql} > ?", [1])
                         ->offset(0)
                         ->limit($limit)
                         ->orderBy('distance', 'ASC')
		                 ->get();
			    	}
				    
                 //userDeviceInfo
                 //userDriverInfo
                 //taxiDriverInfo
                 //taxiTypes
            	            
	          	if((!empty($getDrivers)) && (isset($getDrivers[0])))
	          	{
					$error = false;
	          		foreach ($getDrivers as $drivers) {

          				if($drivers->taxiDriverInfo->taxiTypes->taxi_image){
          					$imagePath = env('APP_URL').'/public/images/taxi/'.$drivers->taxiDriverInfo->taxiTypes->taxi_image;
          				}else{
          					$imagePath = NULL;
          				}
                        
                        if(isset($request->taxi_type_id)){
                            $taxiTypeInfo = TaxiTypes::where(['id' => $request->taxi_type_id])->first();
                        }else{
                            $taxiTypeInfo = TaxiTypes::where(['id' => $drivers->taxiDriverInfo->taxiTypes->id])->first();
                        }
                        $distance = $drivers->distance * 0.621371;
                        $reachingTime = round($drivers->distance);
	          			$data[] = array(
	          					
	          				'user_id' => $drivers->fk_user_id,
	          				'driver_lat' => $drivers->driver_lat,
	          				'driver_lng' => $drivers->driver_lng,
	          				'taxi_id' => $drivers->taxiDriverInfo->id,
	          				'taxi_type_id' => $taxiTypeInfo->id,
	          				'taxi_type' => $taxiTypeInfo->taxi_type,
	          				//'taxi_name' => $taxiTypeInfo->taxi_type,
	          				'base_price' => $taxiTypeInfo->base_price,
	          				'price_per_km' => $taxiTypeInfo->price_per_km,
	          				'taxi_image' => $imagePath,
	          				'capacity' => $taxiTypeInfo->capacity,
	          				'waiting_charges' => $drivers->taxiDriverInfo->taxiTypes->waiting_charges,
	          				'active' => $drivers->taxiDriverInfo->active,
                            'distance' => round($distance),
                            // 'km' => $drivers->distance,
                            'unit' => 'miles',
                            'reachingTime' => $reachingTime
	          			);
	          		}

	          		$response = ['message' => 'Drivers list', 'data' => $data];

	          	}else{

	          		$response = ['message' => 'No Driver found.'];
	          	}
            }else{

          		$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
          	}
		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}



    public function testPayment(){
        // \App\Helpers\Dwolla::getAccessToken();
        \App\Helpers\Dwolla::init();
        echo 'hi'; die();
    }
    
	public function rideRequest(Request $request)
	{
		//this is for driver accept the ride request
        $error = true;
	    $validator = Validator::make($request->all(), [
            'ride_id' => 'required',
            'ride_status' => 'required',
        ]);

		if(!$validator->fails()){
			$error = false;

			$user = Auth::User();
			if($user){
        $driver_stripe = DriverStripe::where(['user_id'=>$user->id])->first();
        if($driver_stripe != null){
          $stripe_id = $driver_stripe->stripe_id;
          \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

         $a = \Stripe\Account::retrieve(
                $stripe_id
            );
            $capabilities = $a['capabilities'];
        }
         if($capabilities->card_payments == 'active' && $capabilities->transfers == 'active'){
                    $getNotifications = Notifications::where(['templateName' =>  'driver-ride-request'])->where('extra', 'LIKE', '%"ride_id":'.$request->ride_id.'%')->where('fk_user_id', '!=', $user->id)->get();

                    if(isset($getNotifications)){
                        foreach($getNotifications as $notifications){
                            $currTime = date("Y-m-d H:i:s");
                            $notifications->deleted_at = $currTime;
                            $notifications->save();
                            // $notifications->delete();
                        }
                    }

					$getRide = RideDetails::where(['id' => $request->ride_id])->first();

					if($getRide){
                            $current_dateTime = date("Y-m-d H:i:s");
                            $from = Carbon::parse($getRide->created_at);
                            $to = Carbon::parse($current_dateTime);
                            $diff_in_seconds = $to->diffInSeconds($from);
                            if($diff_in_seconds > 50){
                                $getRide->ride_status = 'user_expire';
                                $getRide->save();
                            }
						if($getRide->confirmed == '0'){

                                if(($request->ride_status == 'accept') &&($getRide->ride_status != 'user_expire')){

                                    if($request->ride_status == 'accept'){
                                        $getRide->confirmed = '1';
                                        $getRide->ride_status = 'confirmed';

                                        $userData = User::where(['id' => $user->id])->first();
                                        if($userData->isProfileActive == '1'){
                                            $userData->isProfileActive = '0' ;
                                            $userData->save();
                                        }

                                    }else{
                                        $getRide->confirmed = '0';
                                    }
                                        $getRide->fk_driver_id = $user->id;
                                        $getRide->save();
                                            
                                            $getDriverName = User::select(['name'])->where(['id' => $user->id])->first();
                                            $extraPara = ['from' => $getRide->pickup_address, 'to' => $getRide->drop_address,'ride_id' => $getRide->id, 'driverName' => $getDriverName->name];
                                            $requestNotification = $this->sendNotification('driver-accept-request', $getRide->fk_user_id, $extraPara);

                                            $response = ['message' => 'Driver added successfully.',
                                            'rideData' => $this->rideData($getRide->id),
                                            'userData' => $this->userData($getRide->fk_user_id), 
                                            'driverData' => $this->driverData($getRide->fk_driver_id),
                                            'taxiData' => $this->taxiData($getRide->fk_taxi_id),
                                            ];

                                }else{
                                    $error = true;
                                    $response = ['message' => 'Accept time has been expired.'];
                                }
							
						}else{
                            $error = true;
							$response = ['message' => 'Ride already accepted by other driver.'];
						}

					}else{

						$response = ['message' => 'Ride not found'];
					}
             }else{
                $error = true;
                $response = ['message' => "You can't accept the ride as your stripe account is not active. Please update your stripe"];
             }
	       }else{

				$response = ['message' => 'User not found.'];
			}     

        }else{
				$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}

	public function favouriteDriver(Request $request)
	{
		$error = true;
	    $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
            'status' => 'required',
        ]);
		
		if(!$validator->fails()){
			$error = false;

			$user = Auth::User();
			if($user){

					if($request->status == 'add'){
						$favourite = '1';
					}else{
						$favourite = '0';
					}

            		$driverDetail = UserFavouriteDriver::where(['fk_driver_id' => $request->driver_id,'fk_user_id' => $user->id])->first();
		            if($driverDetail){

		            	$driverDetail->favourite = $favourite;
		            	$driverDetail->save();
		            	if($favourite == '1'){
		            		$response = ['message' => 'Driver marked favourite'];
		            	}else{
		            		$response = ['message' => 'Driver marked Unfavourite'];
		            	}

		            }else{
	            		
		            	$newDriver = new UserFavouriteDriver();
		            	$newDriver->fk_user_id = $user->id;
		            	$newDriver->fk_driver_id = $request->driver_id;
		            	$newDriver->favourite = $favourite;
		            	$newDriver->save();

		            	if($favourite == '1'){
		            		$response = ['message' => 'Driver marked favourite'];
		            	}else{
		            		$response = ['message' => 'Driver marked Unfavourite'];
		            	}
		            }

	       }else{

				$response = ['message' => 'User not found.'];
			}     

        }else{
				$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}

	protected function getDriverData($driverID, $userID){
					// echo '<pre>'; print_r($driverInfo->taxiDriverInfo->taxiTypes); die();
            $driver = User::where(['id' => $driverID])->first();
            $user = User::where(['id' => $userID])->first();

            $data = [
                'driverData' => $this->loginDriverData($driverID),
                'userData' => $this->loginData($user),
            ];
			return $data;

	}

	public function favouriteDriverList(Request $request)
	{
		$error = true;
		
		$user = Auth::User();
		if($user){
			
            if($user->user_type = 'user'){
    		  $getDrivers = UserFavouriteDriver::where(['fk_user_id' => $user->id])->get();
            }else{
              $getDrivers = UserFavouriteDriver::where(['fk_driver_id' => $user->id])->get();
            }
            // print_r($getDrivers[0]); die();
            $data = array();
            if(isset($getDrivers[0])){
                $error = false;
            	foreach($getDrivers as $getDriver){
            		$data[] = $this->getDriverData($getDriver->fk_driver_id, $getDriver->fk_user_id);
            	}

            		$response = ['message' => 'Favourite Driver List', 'data' => $data];

            }else{
        		
            	$response = ['message' => 'No Driver Found.'];
            }
        }else{

			$response = ['message' => 'User not found.'];
		}     

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}

	public function advanceRide(Request $request)
	{
		$error = true;
	    $validator = Validator::make($request->all(), [
            'taxi_id' => 'required',
            'ride_type' => 'required',
            'ride_status' => 'required',
            'driver_id' => 'required',
        ]);
		
		if(!$validator->fails()){
			$error = false;

			$user = Auth::User();
			if($user){
		        		if($request->taxi_id)
		        		{
		        			$taxi_info = TaxiTypes::where(['id' => $request->taxi_id])->first();
		        			if($taxi_info)
		        			{
		        				$estimated_cost = $request->distance * $taxi_info->price_per_km;
                                $estimated_cost = round($estimated_cost,2);

		        			}
		        		}

		        		if(empty($estimated_cost))
		        		{
		        			$estimated_cost = "0";
		        		}

		        		$ride = new RideDetails();
		        		$ride->fk_user_id = $user->id;
		        		$ride->fk_taxi_id = $request->taxi_id;
		        		$ride->fk_driver_id = $request->driver_id;
		        		$ride->ride_type = $request->ride_type;
		        		$ride->ride_status = $request->ride_status;
		        		$ride->pickup_address = $request->pickup_address;
		        		$ride->drop_address = $request->drop_address;
		        		$ride->pickup_lat = $request->pickup_lat;
		        		$ride->pickup_lng = $request->pickup_lng;
		        		$ride->drop_lat = $request->drop_lat;
		        		$ride->drop_lng = $request->drop_lng;
		        		$ride->estimated_cost = $estimated_cost;
		        		$ride->actual_cost = $request->actual_cost;
		        		$ride->pay_by = $request->pay_by;
		        		$ride->parcel_height = $request->parcel_height;
		        		$ride->parcel_width = $request->parcel_width;
		        		$ride->parcel_weight = $request->parcel_weight;
		        		$ride->cancel = $request->cancel;
		        		$ride->cancel_reason = $request->cancel_reason;
		        		$ride->advanceRide = '1';

		        		$ride->save();
		        		$response = ['message' => 'Advance ride created successfully.',
							'rideData' => $this->rideData($ride->id),
							'userData' => $this->userData($ride->fk_user_id), 
							'driverData' => $this->driverData($ride->fk_driver_id),
							'taxiData' => $this->taxiData($ride->fk_taxi_id),
							];

	       }else{

				$response = ['message' => 'User not found.'];
			}     

        }else{
				$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}

	public function rideList(Request $request)
	{
		$error = true;
	    $validator = Validator::make($request->all(), [
            //'ride_type' => 'required'
        ]);
		
		if(!$validator->fails()){
			$error = false;
			
			$user = Auth::User();
			if($user){
				$limit = $this->limit;
				if($request->page_number){
					$pageNumber = $request->page_number;
		            if($pageNumber == '1'){
		                $offset = $limit*($pageNumber - 1);
		            }elseif($pageNumber > 1){
		                $offset = $limit*($pageNumber - 1);
		            }else{
		                $limit = '-1';
		                $offset = '0';
		            }
		            $orderBy = 'desc';
		        }
		        elseif($request->last)
		        {
		            $limit = $request->last;
		            $offset = '0';
		            $orderBy = 'desc';
		        }
		        else{
		        	$limit = '99999999';
                    $offset = '0';  
		            $orderBy = 'desc';
		        }
		        	$data = [];
                    if($user->user_type == 'user'){
                        $userType = 'fk_user_id';
                    }elseif($user->user_type == 'driver'){
                        $userType = 'fk_driver_id';
                    }else{
                        $userType = 'fk_user_id';
                    }
                    if(isset($request->ride_type)){
                        $getRides = RideDetails::where([$userType => $user->id, 'ride_type' => $request->ride_type])
                        ->where('ride_status', '!=', 'user_expire')
                        ->offset($offset)
                        ->limit($limit)
                        ->orderBy('id', $orderBy)
                        ->get();    
                    }else{

                        $getRides = RideDetails::where([$userType => $user->id])
                        ->where('ride_status', '!=', 'user_expire')
                        ->offset($offset)
                        ->limit($limit)
                        ->orderBy('id', $orderBy)
                        ->get();
                    }
		        		if(isset($getRides[0])){
		        			$error = false;

		        			foreach($getRides as $getRide){
		        				$data[] = [
		        							'rideData' => $this->rideData($getRide->id),
		        							'userData' => $this->userData($getRide->fk_user_id), 
											'driverData' => $this->driverData($getRide->fk_driver_id,$getRide->fk_user_id),
											'taxiData' => $this->taxiData($getRide->fk_taxi_id),
		        						  ];
		        			}
								$response = ['message' => 'Ride List', 'data' => $data];

		        		}else{
                            $error = true;
		        			$response = ['message' => 'No ride found'];
		        		}

	       }else{
                $error = true;
				$response = ['message' => 'User not found.'];
			}     

        }else{
				$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}

//--------------------------for Drivers------------------------------------------
	public function registerDriver(Request $request)
	{

        $req = json_encode($request->all());
       
        // mail('vasu.bansal@mail.vinove.com','subject',$req);
        // mail('anmol.maheshwari@mail.vinove.com','subject',$req);
		$error = true;
	    $validator = Validator::make($request->all(), [
            // 'name' => 'required', 
            // 'email' => 'required|email',

        ]);
		if(!$validator->fails()){
			$error = false;
			$user = Auth::User();

            $driver = User::where(['id' => $user->id, 'user_type' => 'driver'])->first();
	            if($driver){
	            	$driverID = $driver->id;
	            	if(isset($request->name)){
                        $driver->name = $request->name;
                    }
                    if(isset($request->email)){
                        $driver->email = $request->email;
                    }
                    if(isset($request->isProfileActive)){
                        $driver->isProfileActive = $request->isProfileActive;
                    }
                    if(isset($request->password)){
	            	  $driver->password = Hash::make($request->password);
                    }
	            	$driver->newUser = 'no';
	            	$driver->save();

                    $driver_stripe =DriverStripe::where(['user_id'=>$user->id])->first();

                    if($driver_stripe == null){
                        try{
                     \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

                              $a =  \Stripe\Account::create([
                                      'type' => 'custom',
                                      'country' => 'US',
                                      'email' => $request->email,
                                      'requested_capabilities' => [
                                      'card_payments',
                                       'transfers',
                                  ],
                                ]);

                     $driver_stripe = new DriverStripe;
                     $driver_stripe->user_id = $user->id;
                     $driver_stripe->stripe_id = $a->id;
                     $driver_stripe->save();
                 }catch(\Exception $e){
                    $error = true;
                    $message = $e->getMessage();
                    $response = ['message' => $message];
                 }
                 }

                    if((isset($request->deviceToken)) && (isset($request->deviceId)) && (isset($request->deviceId))){
                            $checkUserDevice = UserDeviceInfo::where(['device_id' => $request->deviceId, 'fk_user_id' => $user->id, 'device_type' => $request->deviceType])->first();
                                    if(!$checkUserDevice)
                                    {
                                        $newUserDevice = new UserDeviceInfo();
                                        $newUserDevice->fk_user_id = $user->id;
                                        $newUserDevice->device_type = $request->deviceType;
                                        $newUserDevice->device_token = $request->deviceToken;
                                        $newUserDevice->device_id = $request->deviceId;
                                        $newUserDevice->save();
                                    }else{
                                        $checkUserDevice->device_token = $request->deviceToken;
                                        $currTime = date("Y-m-d H:i:s");
                                        $checkUserDevice->updated_at = $currTime;
                                        $checkUserDevice->save();
                                    }
                        }

	            	if(($request->driver_image) || ($request->social_security) || ($request->driving_licence)){
	            			$getDriverInfo = DriverInfo::where(['fk_user_id' => $driverID])->first();
	            			if($getDriverInfo){
	            				$driverInfo = $getDriverInfo;
	            			}else{
		            			$driverInfo = new DriverInfo();
	            			}
		            		$driverInfo->fk_user_id = $driverID;
                            if(isset($request->driver_image)){
		            		    $driverInfo->image = $request->driver_image;
                            }
                            if(isset($request->driving_licence)){
                                $driverInfo->driving_licence = $request->driving_licence;
                            }
                            if(isset($request->social_security)){
                                $driverInfo->social_security = $request->social_security;
                            }
	            			$driverInfo->save();
	            	}

    	if($request->taxi_type_id_ride){
    		$getTaxiDriverInfo = TaxiDriverInfo::where(['fk_user_id' => $driverID, 'fk_taxi_type_id' => $request->taxi_type_id_ride])->first();
    		if($getTaxiDriverInfo){
    			$taxiDriverInfo = $getTaxiDriverInfo;	
    		}else{
                
				$taxiDriverInfo = new TaxiDriverInfo();
    		}
			$taxiDriverInfo->fk_user_id = $driverID;
			$taxiDriverInfo->fk_taxi_type_id = $request->taxi_type_id_ride;
            if(isset($request->vehicle_registration_number)){
    			$taxiDriverInfo->vehicle_registration_number = $request->vehicle_registration_number;
            }
            if(isset($request->vehicle_registration_image)){
                $taxiDriverInfo->vehicle_registration_image = $request->vehicle_registration_image;
            }
            if(isset($request->manufacturing_details)){
                $taxiDriverInfo->manufacturing_details = $request->manufacturing_details;
            }
            if(isset($request->tag_number_state)){
                $taxiDriverInfo->tag_number_state = $request->tag_number_state;
            }
            if(isset($request->insurance_company_information)){
                $taxiDriverInfo->insurance_company_information = $request->insurance_company_information;
            }
			$taxiDriverInfo->save();

    	}
          if($request->taxi_type_id_parcel){
            $getTaxiDriverInfo = TaxiDriverInfo::where(['fk_user_id' => $driverID, 'fk_taxi_type_id' => $request->taxi_type_id_parcel])->first();
            if($getTaxiDriverInfo){
                $taxiDriverInfo = $getTaxiDriverInfo;   
            }else{
                $taxiDriverInfo = new TaxiDriverInfo();
            }
               
            $taxiDriverInfo->fk_user_id = $driverID;
            $taxiDriverInfo->fk_taxi_type_id = $request->taxi_type_id_parcel;
            if(isset($request->vehicle_registration_number)){
                $taxiDriverInfo->vehicle_registration_number = $request->vehicle_registration_number;
            }
            if(isset($request->vehicle_registration_image)){
                $taxiDriverInfo->vehicle_registration_image = $request->vehicle_registration_image;
            }
            if(isset($request->manufacturing_details)){
                $taxiDriverInfo->manufacturing_details = $request->manufacturing_details;
            }
            if(isset($request->tag_number_state)){
                $taxiDriverInfo->tag_number_state = $request->tag_number_state;
            }
            if(isset($request->insurance_company_information)){
                $taxiDriverInfo->insurance_company_information = $request->insurance_company_information;
            }
            $taxiDriverInfo->save();

          } 
          
        
	        		$response = ['message' => 'Driver data saved successfully',
	        					 'userData' => $this->loginData($driver),
	        					 'driverData' => $this->loginDriverData($driverID)
	        					];
	            	
	            }else{
	            	$response = ['message' => 'User not found.'];
	            }
        }else {
				$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}

	public function registerDriverAddress(Request $request)
	{
		$error = true;
	    $validator = Validator::make($request->all(), [
            'address' => 'required',
        ]);
		if(!$validator->fails()){
			$error = false;
			$user = Auth::User();
            $driver = User::where(['id' => $user->id, 'user_type' => 'driver'])->first();
	            if($user){
	     			$driverID = $user->id;
	        		if($request->driver_address_id){
	        			$getDriverAddressInfo = DriverAddressInfo::where(['fk_user_id' => $user->id])->first();
                        if(isset($getDriverAddressInfo)){
                            $driverAddressInfo = $getDriverAddressInfo;     
                        }else{
                            $response = ['message' => 'Driver Address is incorrect.'];
                            return response()->json(['response' => $response], $this->errorCode);
                        }
	        			
	        		}else{
	    				$driverAddressInfo = new DriverAddressInfo();
	        		}
	    			$driverAddressInfo->fk_user_id = $driverID;
	    			$driverAddressInfo->address = $request->address;
	    			$driverAddressInfo->city = $request->city;
	    			$driverAddressInfo->state = $request->state;
	    			$driverAddressInfo->pincode = $request->pincode;
	    			$driverAddressInfo->save();

	        		$response = ['message' => 'Driver data saved successfully',
	        					 'userData' => $this->loginData($driver),
	        					 'driverData' => $this->loginDriverData($driverID)
	        					];
	            	
	            }else{
	            	$response = ['message' => 'User not found.'];
	            }
        }else {
				$messages = $validator->messages();
	            $allMessage = [];
		            foreach ($messages->all() as $message){
					    $allMessage[] = $message;
					}
            	$response = ['message' => $allMessage];
		}

		if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
	}

    public function addCreditBalance(Request $request)
    {
        $error = true;
        $validator = Validator::make($request->all(), [
            'wallet_amount' => 'required',
        ]);
        
        if(!$validator->fails()){
            $error = false;

            $user = Auth::User();
            if($user){

                if($request->wallet_amount){

                    $getWallet = UserWallet::where(['fk_user_id' => $user->id])->first();
                    if($getWallet)
                    {
                        $newWallet = $getWallet->wallet_amount + $request->wallet_amount;
                        $userWallet = $getWallet;
                    }else{
                        $newWallet = $request->wallet_amount;
                        $userWallet = new UserWallet();
                    }

                    $userWallet->fk_user_id = $user->id;
                    $userWallet->wallet_amount = $newWallet;
                    $userWallet->save();

                    $response = ['message' => 'Credit added successfully', 'wallet_amount' => $userWallet->wallet_amount];
                }                

           }else{

                $response = ['message' => 'User not found.'];
            }     

        }else{
                $messages = $validator->messages();
                $allMessage = [];
                    foreach ($messages->all() as $message){
                        $allMessage[] = $message;
                    }
                $response = ['message' => $allMessage];
        }

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
    }

    public function viewCreditBalance(Request $request)
    {
        $error = true;
        $user = Auth::User();
        if($user){
          
            $getWallet = UserWallet::where(['fk_user_id' => $user->id])->first();
            if($getWallet)
            {   
                $wallet_amount = $getWallet->wallet_amount; 
            }else{
                $wallet_amount = '0';
            }
            $error = false;
            $response = ['message' => 'Credit Balance', 'wallet_amount' => $wallet_amount];             

       }else{
            $response = ['message' => 'User not found.'];
        }     

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
    }

    public function driverProfile(Request $request)
    {
        $error = true;
        $user = Auth::User();
        if($user){

            $getDriverData = $this->loginDriverData($user->id);
            $driver = User::where(['id' => $user->id, 'user_type' => 'driver'])->first();
            $error = false;
            $drivers = TaxiDriverInfo::where(['fk_user_id'=>$user->id])->get();
            $taxi_id=[];
            if($drivers){
                if($drivers->count() == 2){
                    
                   foreach($drivers as $drive){
                    $taxi_id[]= $drive->fk_taxi_type_id;
                   }
                     // dd($taxi_id[1]);
                   $taxi_type=TaxiTypes::whereIn('id',[$taxi_id[0],$taxi_id[1]])->get();
                  //  dd( $taxi_type[0]->taxi_type,$taxi_type[1]->taxi_type);
                   
                  
                  $taxiNameToDisplay=$taxi_type[0]->taxi_type.', '.$taxi_type[1]->taxi_type;

                }
            }

            if(!isset($taxiNameToDisplay)){
               $taxiNameToDisplay=null;
            }
            $response = ['message' => 'Driver data',
                         'userData' => $this->loginData($driver),
                         'driverData' => $this->loginDriverData($user->id),
                         'taxiNameToDisplay'=>$taxiNameToDisplay
                        ];
       }else{
            $response = ['message' => 'Driver not found.'];
        }     

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
    }

    public function updateDriverLatLng(Request $request)
    {
        $error = true;
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longitude' => 'required',
        ]);
        
        if(!$validator->fails()){

            $user = Auth::User();
            if($user){
                $getDriver = DriverInfo::where(['fk_user_id' => $user->id])->first();
                if($getDriver){
                    $getDriver->driver_lat = $request->latitude;
                    $getDriver->driver_lng = $request->longitude;
                    $getDriver->speed = (isset($request->speed)) ? $request->speed : NULL; 
                    if($getDriver->save()){
                        $error = false;
                        $response = ['message' => 'Driver Lat Lng has been updated successfully.'];
                    }else{
                        $response = ['message' => 'Some problem occured.'];
                    }
            }else{
                    $response = ['message' => 'Driver not found.'];
            }
           }else{
                    $response = ['message' => 'Driver not found.'];
            }     

        }else{
                $messages = $validator->messages();
                $allMessage = [];
                    foreach ($messages->all() as $message){
                        $allMessage[] = $message;
                    }
                $response = ['message' => $allMessage];
        }

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
    }


    public function getDriverLatLng(Request $request)
    {
        $error = true;
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
        ]);

        if(!$validator->fails()){

            if($request->driver_id){
                $getDriver = DriverInfo::where(['fk_user_id' => $request->driver_id])->first();
                if($getDriver){
                 $error = false;
                 $data = array(
                    'latitude' => $getDriver['driver_lat'],
                    'longitude' => $getDriver['driver_lng'],
                    'speed' => $getDriver['speed']
                 );

                 $response = ['message' => 'Lat Lng data', 'data' => $data];
                    
            }else{
                    $response = ['message' => 'Driver not found.'];
            }
           }else{
                    $response = ['message' => 'Driver not found.'];
            }    

        }else{
                $messages = $validator->messages();
                $allMessage = [];
                    foreach ($messages->all() as $message){
                        $allMessage[] = $message;
                    }
                $response = ['message' => $allMessage];
        }

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
    }

    public function driverRating(Request $request)
    {
        $error = true;
        $validator = Validator::make($request->all(), [
            'rating' => 'required',
            'ride_id' => 'required',
            'rating_to_id' => 'required',

        ]);
        
        if(!$validator->fails()){

            $user = Auth::User();
            if($user){
                // dd($user);
                if($user->user_type == 'user'){
                    
                    $rating = new DriverRating();
                    $rating->fk_user_id = $user->id;
                    $rating->fk_driver_id = $request->rating_to_id;
                    $rating->fk_ride_id = $request->ride_id;
                    $rating->rating = $request->rating;
                    $rating->comment = urldecode($request->comment);
                }elseif($user->user_type == 'driver'){
                    $rating = new UserRating();
                    $rating->fk_driver_id = $user->id;
                    $rating->fk_user_id = $request->rating_to_id;
                    $rating->fk_ride_id = $request->ride_id;
                    $rating->rating = $request->rating;
                    $rating->comment = urldecode($request->comment);
                }
                    
                if($rating->save()){
                    $error = false;
                    $response = ['message' => 'Rating has been inserted successfully.'];
                }else{
                    $response = ['message' => 'Logged in user is Admin.'];
                }
            
           }else{
                    $response = ['message' => 'User not found.'];
            }     
        }else{
                $messages = $validator->messages();
                $allMessage = [];
                    foreach ($messages->all() as $message){
                        $allMessage[] = $message;
                    }
                $response = ['message' => $allMessage];
        }

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
    }

    public function ridePayment(Request $request)
    {
        $error = true;
        $validator = Validator::make($request->all(), [
            //'pay_by' => 'required',
            'ride_id' => 'required',
           // 'actual_cost' => 'required'
        ]);
        
        if(!$validator->fails()){

            $user = Auth::User();
            if($user){
                /*if($request->pay_by == 'cash'){

                    $getRide = RideDetails::where(['id' => $request->ride_id])->first();

                    if($getRide){
                        
                        $getRide->pay_by = 'cash';
                        $getRide->payment_status = 'paid';
                        $getRide->save();
                        $error = false;

                        $extraPara = ['ride_id' => $request->ride_id, 'money' => $request->actual_cost];
                        $requestNotification = $this->sendNotification('payment-cash', $getRide->fk_user_id, $extraPara);

                        $response = ['message' => 'Payment successfully done.'];

                    }else{
                        $response = ['message' => 'Ride not found'];
                    }

                }else*///if($request->pay_by == 'wallet'){
                                     
                        $getRide = RideDetails::where(['id' => $request->ride_id])->first();
                        if($getRide){
                         /*
                            $getUserWallet = UserWallet::where(['fk_user_id' => $getRide->fk_user_id])->first();*/
                           /* if($getUserWallet)
                            {*/
                            /*    $wallet_amount = $getUserWallet->wallet_amount;
                                if((float)$request->actual_cost > (float)$wallet_amount){
                                    
                                    $error = true;
                                    $response = ['message' => 'Insufficient Balance','code' => $this->errorCode];    
                                    return response()->json(['response' => $response]);  
                                }else{
                                    
                                    $error = false;
                                    $new_wallet_amount = (float)$wallet_amount - (float)$request->actual_cost;
                                    $getUserWallet->wallet_amount = $new_wallet_amount;
                                    $getUserWallet->save();
                                    
                                    $getRide = RideDetails::where(['id' => $request->ride_id])->first();*/
                                    if($getRide->payment_status == 'paid'){
                                     $error= false;

                                    $response = ['message' => 'Payment successfully done.'];

                                    $userData = User::where(['id' => $user->id])->first();
                                        if($userData->isProfileActive == '0'){
                                            $userData->isProfileActive = '1' ;
                                            $userData->save();
                                        }
                                    
                                    /*$extraPara = ['ride_id' => $request->ride_id, 'money' => $request->actual_cost];
                                    //dd($extraPara);
                                    $requestNotification = $this->sendNotification('payment-wallet', $getRide->fk_user_id, $extraPara);
                                    $requestNotification = $this->sendNotification('payment-wallet-to-driver', $getRide->fk_driver_id, $extraPara);*/
                                }

                           else{
                              $error = true;
                              $response = ['message' => 'Payment Not Done','code' => $this->errorCode];
                              return response()->json(['response' => $response]);
                            }
                        }else{
                            $error = true;
                            $response = ['message' => 'Ride not found','code' => $this->errorCode];
                            return response()->json(['response' => $response]); 
                        }
                //}
            
           }else{
                    $response = ['message' => 'User not found.'];
            }     
        }else{
                $messages = $validator->messages();
                $allMessage = [];
                    foreach ($messages->all() as $message){
                        $allMessage[] = $message;
                    }
                $response = ['message' => $allMessage];
        }

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
    }

    public function paymentStatus(Request $request){
        $error = true;
        $validator = Validator::make($request->all(), [
            'status' => 'required',
            'ride_id' => 'required'
        ]);
        
        if(!$validator->fails()){

            $user = Auth::User();
            if($user){
               if($request->status == 'Succeeded' || $request->status == 'succeeded'){
                        $commission = Commission::first();             
                        $getRide = RideDetails::where(['id' => $request->ride_id])->first();
                        if($getRide){
                               if($getRide->actual_cost){
                                if($getRide->tips){
                                    $actual_cost_dollar = $getRide->actual_cost;
                                    $actual_cost_dollar = $actual_cost_dollar + $getRide->tips;
                                    $actual_cost_dollar = round($actual_cost_dollar,2);
                                }else{
                                $actual_cost_dollar = $getRide->actual_cost;
                                $actual_cost_dollar = round($actual_cost_dollar,2);
                                }
                                $admin_money = ($getRide->actual_cost)*($commission->admin_commission/100);
                                $admin_money = round($admin_money,2);
                                $driver_money = $actual_cost_dollar - $admin_money;
                                $money_split_detail = MoneySplitDetail::where(['ride_id'=>$request->ride_id])->first();
                                if($money_split_detail == null){
                                  $money_split_detail = new MoneySplitDetail; 
                                }
                                $money_split_detail->driver_money = $driver_money;
                                $money_split_detail->admin_money = $admin_money;
                                $money_split_detail->driver_id =  $getRide->fk_driver_id;
                                $money_split_detail->ride_id = $getRide->id;
                                 $money_split_detail->save();
                             }

                                    $error = false;
                                    $getRide = RideDetails::where(['id' => $request->ride_id])->first();
                                    if($getRide->payment_status != 'paid'){
                                    $getRide->payment_status = 'paid';
                                    $getRide->save();

                                    $response = ['message' => 'Payment successfully done.'];
                                    $userName = $getRide->userInfo->name;
                                    $extraPara = ['ride_id' => $request->ride_id, 'money' => $actual_cost_dollar,'user_name'=>$userName];
                                    //dd($extraPara);
                                    $requestNotification = $this->sendNotification('payment-wallet', $getRide->fk_user_id, $extraPara);
                                    $requestNotification = $this->sendNotification('payment-wallet-to-driver', $getRide->fk_driver_id, $extraPara);
                                }

                        }else{
                            $error = true;
                            $response = ['message' => 'Ride not found','code' => $this->errorCode];
                            return response()->json(['response' => $response]); 
                        }
                        }else{
                              $error = true;
                              $response = ['message' => 'Payment Not Completed','code' => $this->errorCode];
                              return response()->json(['response' => $response]);
                            }
            
           }else{
                    $response = ['message' => 'User not found.'];
            }     
        }else{
                $messages = $validator->messages();
                $allMessage = [];
                    foreach ($messages->all() as $message){
                        $allMessage[] = $message;
                    }
                $response = ['message' => $allMessage];
        }

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);

    }


    public function readNotification(Request $request)
    {
        $error = true;
        $validator = Validator::make($request->all(), [
            'push_id' => 'required',
        ]);
        
        if(!$validator->fails()){

            $user = Auth::User();
            if($user){
                if($user->user_type == 'user'){
                    $getNotif = Notifications::where(['fk_user_id' => $user->id, 'messageRead' => '0'])->get();
                    if($getNotif){
                        foreach($getNotif as $getNotifica){
                            $getNotifica->messageRead = '1';
                            $getNotifica->save();
                        }                
                            $error = false;
                            $response = ['message' => 'All Notifications are read successfully.'];
                        
                    }else{
                        $error = false;
                        $response = ['message' => 'All Notifications are read successfully.'];
                    }
                }else{
                    $getNotif = Notifications::where(['id' => $request->push_id])->first();
                    if($getNotif){
                        
                            $getNotif->messageRead = '1';
                            $getNotif->save();

                            $error = false;
                            $response = ['message' => 'Notifications are read successfully.'];
                    }
                }
            
           }else{
                    $response = ['message' => 'User not found.'];
                }

        }else{
                $messages = $validator->messages();
                $allMessage = [];
                    foreach ($messages->all() as $message){
                        $allMessage[] = $message;
                    }
                $response = ['message' => $allMessage];
        }

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
    }

    public function getAllFaqs(Request $request)
    {
        $error = true;
        $user = Auth::User();
        if($user)
        {
            $arrFaqDriver=FAQ::where(['status'=>1])->where(['faq_uses'=>'driver'])->get();
            $arrFaqUser=FAQ::where(['status'=>1])->where(['faq_uses'=>'user'])->get();
            if($user->user_type == 'user'){
            
            if($arrFaqUser->count()!=0)
            {
                $error=false;
                foreach($arrFaqUser as $faq) {

                    $created_date = $faq->created_at; 
                    $new_created_date = date("d-m-Y", strtotime($created_date));  

                    $updated_at = $faq->created_at; 
                    $new_updated_at = date("d-m-Y", strtotime($updated_at));  

                    $data[]= array(
                    'question'=>$faq->question,
                    'answer'=>$faq->answer,
                    'faq_for'=>$faq->faq_uses,
                    'created_at'=>$new_created_date,
                    'updated_at'=>$new_updated_at
                    );
                }
                $response = ['message' => 'Faq list', 'data' => $data];
            }
            else {
                $response = ['message' => 'FAQs not found.'];
            }
         }

         else if($user->user_type == 'driver'){
            
            if($arrFaqDriver->count()!=0)
            {
                $error=false;
                foreach($arrFaqDriver as $faq) {

                    $created_date = $faq->created_at; 
                    $new_created_date = date("d-m-Y", strtotime($created_date));  

                    $updated_at = $faq->created_at; 
                    $new_updated_at = date("d-m-Y", strtotime($updated_at));  

                    $data[]= array(
                    'question'=>$faq->question,
                    'answer'=>$faq->answer,
                    'faq_for'=>$faq->faq_uses,
                    'created_at'=>$new_created_date,
                    'updated_at'=>$new_updated_at
                    );
                }
                $response = ['message' => 'Faq list', 'data' => $data];
            }
            else {
                $response = ['message' => 'FAQs not found.'];
            }
         }


        } else {
            $response = ['message' => 'User not found.'];
        }

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }
      
        return response()->json(['response' => $response]);
    }

   public function userDetail(Request $request){
       $error = true;
       
        $user = Auth::User();
       
            if($user->user_type =='user'){
            
           
                   $error = false;
                  
                       $response = [
                                
                                'userData' => $this->userData($user->id), 
                                 
                                /*'Share' => array( 'driver_fare' =>$driverFare,
                                                   'admin_fare' =>$adminFare
                                )*/
                                ];


           }else{
                    $response = ['message' => 'User not found.'];
                }

        

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
    }

/*        public function getTaxiTypesInfo(Request $request)
    {
        $error = true;
        if($request->taxi_uses=="ride"){
            $getTaxis = TaxiTypes::where(["taxi_uses"=>'ride'])->get();
            $getTaxisCount=$getTaxis->count();
            if($getTaxisCount!=0)
            {
                $error = false;
                foreach ($getTaxis as $getTaxi) {

                    if($getTaxi->taxi_image){
                        $imagePath = env('APP_URL').'/public/images/taxi_type/'.$getTaxi->taxi_image;
                    }else{
                        $imagePath = NULL;
                    }

                    $data[] = array(
                        'taxi_type_id' => $getTaxi->id,
                        'taxi_type' => $getTaxi->taxi_type,
                        'taxi_name' => $getTaxi->taxi_name,
                        'price_per_km' => $getTaxi->price_per_km,
                        'taxi_image' => $imagePath,
                        'capacity' => $getTaxi->capacity,
                        'waiting_charges' => $getTaxi->waiting_charges,
                        'base_price' => $getTaxi->base_price,
                        'active' => $getTaxi->active,
                    );
                }

                $response = ['message' => 'Taxi list', 'data' => $data];

            }else{

                $response = ['message' => 'No taxi found'];
            }

          }
        elseif($request->taxi_uses=="parcel"||$request->taxi_uses=="both"){
            $getTaxis = TaxiTypes::where(["taxi_uses"=>'parcel'])->where(["taxi_uses"=>'p'])->get();
            $getTaxisCount=$getTaxis->count();
            if($getTaxisCount!=0)
            {
                $error = false;
                foreach ($getTaxis as $getTaxi) {

                    if($getTaxi->taxi_image){
                        $imagePath = env('APP_URL').'/public/images/taxi_type/'.$getTaxi->taxi_image;
                    }else{
                        $imagePath = NULL;
                    }

                    $data[] = array(
                        'taxi_type_id' => $getTaxi->id,
                        'taxi_type' => $getTaxi->taxi_type,
                        'taxi_name' => $getTaxi->taxi_name,
                        'price_per_km' => $getTaxi->price_per_km,
                        'taxi_image' => $imagePath,
                        'capacity' => $getTaxi->capacity,
                        'waiting_charges' => $getTaxi->waiting_charges,
                        'base_price' => $getTaxi->base_price,
                        'active' => $getTaxi->active,
                    );
                }

                $response = ['message' => 'Taxi list', 'data' => $data];

            }else{

                $response = ['message' => 'No taxi found'];
            }

        }

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
    }*/
    public function lastRide(Request $request)
    {   
            $user = Auth::User();
            if($user){
                 $getRides = RideDetails::where(['fk_user_id' => $user->id])
                        ->orderBy('id', 'DESC')->first();  
                    if($getRides){
                        if($getRides->confirmed == '1' &&  $getRides->payment_status == 'unpaid'){
                            $error = false;

                                $data[] = [
                                            'rideData' => $this->rideData($getRides->id),
                                            'userData' => $this->userData($getRides->fk_user_id), 
                                            'taxiData' => $this->taxiData($getRides->fk_taxi_id),
                                          ];
                        
                                $response = ['message' => 'Ride List', 'data' => $data];

                        }else{
                            $error = true;
                            $response = ['message' => 'No ride found'];
                        }
                    }else{
                         $error = true;
                            $response = ['message' => 'No ride found'];
                    }

           }else{
                $error = true;
                $response = ['message' => 'User not found.'];
            }     

    

        if($error){
            $response['code'] = $this->errorCode;
        }else{
            $response['code'] = $this->successCode;
        }

        return response()->json(['response' => $response], $response['code']);
    }


}
